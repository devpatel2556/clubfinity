import React from 'react';
import {
  StyleSheet, Text,
} from 'react-native';
import PropTypes from 'prop-types';
import {
  actions,
  RichEditor,
  RichToolbar,
} from 'react-native-pell-rich-editor';
import colors from '../../util/colors';
import newColors from '../../constants/Colors';
import LabelAndErrorContainer from './LabelAndErrorContainer';

const styles = StyleSheet.create({
  textInput: {
    // color: "purple",
    // backgroundColor: "purple"
    // This is required to avoid content jump when an error is added
    // borderWidth: 1,
    // borderRadius: 10,
    // padding: 13,
    // paddingLeft: 20,
    // width: '100%',
    color: colors.inputText,
    // fontSize: 16,

    // Separate paddingTop style is required to overwrite the paddingTop multiline style within react native
    // paddingTop: 13,
    // marginBottom: 40,

    // height: 200,
    // textAlignVertical: 'top',
  },
  errorInput: {
    borderColor: colors.error,
    borderWidth: 1,
  },
  /** ***************************** */
  /* styles for html tags */
  a: {
    fontWeight: 'bold',
    color: 'purple',
  },
  div: {
    fontFamily: 'monospace',
  },
  p: {
    fontSize: 30,
  },
  /** **************************** */
  container: {
    flex: 1,
    marginTop: 40,
  },
  richContainer: {
    borderRadius: 5,
  },
  RichEditor: {
    backgroundColor: newColors.mutedWhite,
    color: newColors.black,
  },
  rich: {
    minHeight: 50,
    flex: 1,
    marginBottom: 5,
    borderRadius: 5,
  },
  richBar: {
    backgroundColor: 'transparent',
    marginTop: 10,
  },

});

export default class extends React.Component {
  static propTypes = {
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    placeholder: PropTypes.string,
    label: PropTypes.string,
    error: PropTypes.string,
    RichText: PropTypes.instanceOf(Object).isRequired,
    tintColor: PropTypes.string,
  };

  static defaultProps = {
    placeholder: null,
    label: null,
    error: null,
    tintColor: null,
  };

  render() {
    const {
      RichText,
      placeholder,
      value,
      onChange,
      label,
      error,
    } = this.props;

    let tempPlaceHolder = placeholder;
    let tempContent = '';
    if (value.length !== 0) {
      tempPlaceHolder = value;
      tempContent = value;
    }
    return (
      <LabelAndErrorContainer
        style={styles.rich}
        label={label}
        error={error}
      >

        {/* <TextInput
          style={{ ...styles.textInput, ...(error ? styles.errorInput : {}) }}
          multiline
          placeholder={placeholder}
          placeholderTextColor={colors.inputPlaceholder}
          returnKeyType="done"
          value={value}
          onChangeText={onChange}
          {...rest}
        /> */}
        <RichToolbar
          style={[styles.richBar]}
          editor={RichText}
          disabled={false}
          iconTint={newColors.blue}
          selectedIconTint={newColors.blueFade}
          disabledIconTint={newColors.blue}
        // onPressAddImage={onPressAddImage}
          iconSize={40}
        // map icons for self made actions
          iconMap={{
            [actions.heading1]: ({ tintColor }) => (
              <Text style={[styles.tib, { color: tintColor }]}>H1</Text>
            ),
          }}
        />
        {/* <HTMLView value={tempPlaceHolder} style={styles.rich}/> */}
        <RichEditor
          androidHardwareAccelerationDisabled
          androidLayerType="software"
        // containerStyle={styles.richContainer}
          containerStyle={{ ...styles.richContainer, ...(error ? styles.errorInput : {}) }}
          ref={RichText}
          editorStyle={styles.RichEditor}
          placeholder={tempPlaceHolder}
          initialContentHTML={tempContent}
          onChange={onChange}
        />
      </LabelAndErrorContainer>
    );
  }
}
