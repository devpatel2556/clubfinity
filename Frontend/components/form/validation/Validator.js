import {
  isValidFacebookUrl, isValidInstagramUrl, isValidSlackUrl, isValidUrl,
} from '../../../util/validationUtil';

// Standard Email regex, see: https://emailregex.com/
// eslint-disable-next-line max-len
const EMAIL_REGEX = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export default class Validator {
  static required(errorMessage = 'Required') {
    return (value) => {
      if (value === null || value === '') {
        return errorMessage;
      }

      return null;
    };
  }

  static email(errorMessage = 'Invalid') {
    return (value) => {
      if (!EMAIL_REGEX.test(value)) {
        return errorMessage;
      }

      return null;
    };
  }

  static validUrl(errorMessage = 'Invalid') {
    return (value) => {
      if (!isValidUrl(value)) {
        return errorMessage;
      }

      return null;
    };
  }

  static validFacebookUrl(errorMessage = 'Invalid') {
    return (value) => {
      if (value !== '' && !isValidFacebookUrl(value)) {
        return errorMessage;
      }

      return null;
    };
  }

  static validInstagramUrl(errorMessage = 'Invalid') {
    return (value) => {
      if (!isValidInstagramUrl(value)) {
        return errorMessage;
      }

      return null;
    };
  }

  static validSlackUrl(errorMessage = 'Invalid') {
    return (value) => {
      if (!isValidSlackUrl(value)) {
        return errorMessage;
      }

      return null;
    };
  }

  static minLength(minLength, errorMessage = 'Too Short') {
    return (value) => {
      const { length } = value;
      if (length < minLength) {
        return errorMessage;
      }

      return null;
    };
  }

  static maxLength(maxLength, errorMessage = 'Too Long') {
    return (value) => {
      const { length } = value;
      if (length > maxLength) {
        return errorMessage;
      }

      return null;
    };
  }

  static regex(regex, errorMessage = 'Invalid') {
    return (value) => {
      if (!regex.test(value)) {
        return errorMessage;
      }

      return null;
    };
  }

  static numeric(errorMessage = 'Invalid') {
    return (value) => {
      if (Number.isNaN(Number(value))) {
        return errorMessage;
      }

      return null;
    };
  }

  static endsWith(suffix, errorMessage = 'Invalid') {
    return (value) => {
      // eslint-disable-next-line react/destructuring-assignment
      if (!value.endsWith(suffix)) {
        return errorMessage;
      }

      return null;
    };
  }

  static equalsField(field, errorMessage = 'Invalid') {
    return (value, formValue) => {
      // eslint-disable-next-line react/destructuring-assignment
      if (value !== formValue[field]) {
        return errorMessage;
      }

      return null;
    };
  }

  constructor(config) {
    this.config = config;
  }

  validate(formValue) {
    const errors = {};

    Object.keys(this.config).forEach((field) => {
      const validators = this.config[field];
      const value = formValue[field];

      validators.forEach((validator) => {
        const error = validator(value, formValue);

        if (error && !errors[field]) {
          errors[field] = error;
        }
      });
    });

    return { valid: Object.keys(errors).length === 0, errors };
  }
}
