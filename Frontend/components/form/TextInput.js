import React from 'react';
import {
  StyleSheet, TextInput,
} from 'react-native';
import { ViewPropTypes } from 'deprecated-react-native-prop-types';
import PropTypes from 'prop-types';
import colors from '../../util/colors';
import LabelAndErrorContainer from './LabelAndErrorContainer';

const styles = StyleSheet.create({
  textInput: {
    // This is required to avoid content jump when an error is added
    borderWidth: 1,
    borderColor: 'transparent',
    borderRadius: 10,
    backgroundColor: colors.grayScale1,
    padding: 13,
    paddingLeft: 20,
    width: '100%',
    color: colors.inputText,
    fontSize: 16,
  },
  errorInput: {
    borderColor: colors.error,
    borderWidth: 1,
  },
});

export default class extends React.Component {
    static propTypes = {
      value: PropTypes.string.isRequired,
      onChange: PropTypes.func.isRequired,
      placeholder: PropTypes.string,
      label: PropTypes.string,
      style: ViewPropTypes.style,
      error: PropTypes.string,
    };

    static defaultProps = {
      placeholder: null,
      label: null,
      style: {},
      error: null,
    };

    render() {
      const {
        placeholder, value, onChange, label, style, error, ...rest
      } = this.props;

      return (
        <LabelAndErrorContainer
          style={style}
          label={label}
          error={error}
        >
          <TextInput
            style={{ ...styles.textInput, ...(error ? styles.errorInput : {}) }}
            placeholder={placeholder}
            placeholderTextColor={colors.inputPlaceholder}
            returnKeyType="done"
            value={value}
            onChangeText={onChange}
            /* eslint-disable-next-line react/jsx-props-no-spreading */
            {...rest}
          />
        </LabelAndErrorContainer>
      );
    }
}
