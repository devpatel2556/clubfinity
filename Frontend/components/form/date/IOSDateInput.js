import React from 'react';
import { StyleSheet, View } from 'react-native';
import PropTypes from 'prop-types';
// eslint-disable-next-line
import DateTimePicker from '@react-native-community/datetimepicker';
import { DateTime } from 'luxon';
import { DATE_PICKER_FORMAT } from '../../../util/dateUtil';

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  datePicker: {
    minWidth: 90,
    padding: 20,
    fontSize: 24,
    marginHorizontal: 10,
  },
});

export default class IOSDateInput extends React.Component {
  static propTypes = {
    value: PropTypes.instanceOf(DateTime).isRequired,
    onChange: PropTypes.func.isRequired,
  };

  componentDidMount() {
    const { onChange, value } = this.props;
    // This is needed since the IOS datepicker must have a valid date,
    // whereas the android date picker can have a null date
    if (!value) {
      onChange(DateTime.local().startOf('minute'));
    }
  }

  onValueChange = (event, date) => {
    const { onChange } = this.props;

    const dateTime = DateTime.fromJSDate(date).startOf('minute');

    onChange(dateTime);
  }

  render() {
    const { value } = this.props;

    // This is required to prevent null pointer exceptions for the split second before componentDidMount is called
    if (!value) {
      return <></>;
    }

    return (
      <View style={styles.container}>
        <DateTimePicker
          mode="date"
          style={styles.datePicker}
          value={value.toJSDate()}
          format={DATE_PICKER_FORMAT}
          onChange={this.onValueChange}
        />
        <DateTimePicker
          mode="time"
          style={styles.datePicker}
          value={value.toJSDate()}
          format={DATE_PICKER_FORMAT}
          onChange={this.onValueChange}
        />
      </View>
    );
  }
}
