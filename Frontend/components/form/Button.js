import React from 'react';
import {
  TouchableOpacity, Text, StyleSheet,
} from 'react-native';
import { ViewPropTypes } from 'deprecated-react-native-prop-types';
import PropTypes from 'prop-types';

import colors from '../../util/colors';

const styles = StyleSheet.create({
  button: {
    padding: 15,
    backgroundColor: colors.primary0,
    borderRadius: 8,
    minHeight: 42,
    elevation: 3,
  },
  text: {
    fontSize: 15,
    alignSelf: 'center',
    color: colors.grayScale0,
  },
});

const Button = (props) => {
  const { text, onPress, style } = props;

  return (
    <TouchableOpacity
      style={{ ...styles.button, ...style }}
      onPress={onPress}
    >
      <Text style={styles.text}>{text}</Text>
    </TouchableOpacity>
  );
};

Button.propTypes = {
  text: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
  style: ViewPropTypes.style,
};

Button.defaultProps = {
  style: {},
};

export default Button;
