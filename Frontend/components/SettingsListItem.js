import React from 'react';
import { View } from 'react-native';
import { Text, HStack, Pressable } from 'native-base';
import { Ionicons } from '@expo/vector-icons';
import PropTypes from 'prop-types';

export default class SettingsListItem extends React.Component {
  static propTypes = {
    onPress: PropTypes.func.isRequired,
    label: PropTypes.string.isRequired,
    icon: PropTypes.string.isRequired,
  };

  render() {
    const { onPress, label, icon } = this.props;

    return (
      <Pressable
        onPress={onPress}
      >
        {(({ isPressed, isHovered }) => (
          <HStack
            justifyContent="space-between"
            width="100%"
            padding="4"
            borderBottomWidth="1"
            borderBottomColor="gray.300"
            bg={isPressed ? 'gray.300' : isHovered ? 'coolGray.200' : 'white'}
          >
            <View
              style={{
                display: 'flex',
                flexDirection: 'row',
                flex: 1,
                justifyContent: 'space-between',
                alignItems: 'center',
                height: '100%',
              }}
            >
              <View
                style={{
                  display: 'flex',
                  flexDirection: 'row',
                }}
              >
                <Ionicons
                  name={icon}
                  size={20}
                  style={{ paddingRight: '2%' }}
                />
                <Text>{label}</Text>
              </View>
              <Ionicons name="chevron-forward-outline" size={30} />
            </View>
          </HStack>
        ))}
      </Pressable>
    );
  }
}
