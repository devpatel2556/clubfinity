/* eslint-disable react/jsx-props-no-spreading,react/prop-types */

import React, { useRef, useState } from 'react';
import {
  View, StyleSheet, Image, TouchableOpacity, ActivityIndicator,
} from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import { Feather } from '@expo/vector-icons';
import TextInput from './form/TextInput';
import SelectField from './form/SelectInput';
import TextAreaInput from './form/TextAreaInput';
import Button from './form/Button';
import DateInput from './form/date/DateInput';
import defaultClubImage from '../assets/images/DefaultClubImage.png';
import defaultProfileImage from '../assets/images/DefaultUserImage.png';

const styles = StyleSheet.create({
  container: {
    paddingLeft: '5%',
    paddingRight: '5%',
    paddingTop: 10,
  },
  item: {
    marginTop: 11,
    marginBottom: 11,
  },
  imageContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 10,
  },
  imageViewContainer: {
    height: 120,
    width: 120,
    borderRadius: 60,
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    height: 120,
    width: 120,
    position: 'absolute',
  },
  imageOverlay: {
    backgroundColor: 'rgba(0,0,0, 0.3)',
    ...StyleSheet.absoluteFill,
  },
});

export default class Form extends React.Component {
  render() {
    return (
      <View style={styles.container} ref={(c) => this._root = c} {...this.props} />
    );
  }
}

Form.Text = (props) => {
  const { style, ...rest } = props;

  return (
    <TextInput style={{ ...styles.item, ...style }} {...rest} />
  );
};
Form.TextArea = (props) => {
  const RichText = useRef();

  const { style, ...rest } = props;
  return (
    <TextAreaInput
      style={{ ...styles.item, ...style }}
      RichText={RichText}
      {...rest}
    />
  );
};

Form.Select = (props) => {
  const { style, ...rest } = props;

  return (
    <SelectField style={{ ...styles.item, ...style }} {...rest} />
  );
};

Form.Date = (props) => {
  const { style, ...rest } = props;

  return (
    <DateInput style={{ ...styles.item, ...style }} {...rest} />
  );
};

Form.Button = (props) => {
  const { style, ...rest } = props;

  return (
    <Button style={{ ...styles.item, ...style }} {...rest} />
  );
};

Form.ImagePicker = (props) => {
  const { image, onImagePicked, type } = props;
  const [loading, setLoading] = useState(true);

  const pickImage = async () => {
    setLoading(true);

    const result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: false,
      aspect: [1, 1],
      quality: 1,
    });

    setLoading(false);

    if (!result.canceled) {
      onImagePicked(result.assets[0].uri);
    }
  };

  return (
    <View style={styles.imageContainer}>
      <TouchableOpacity style={styles.imageViewContainer} onPress={() => pickImage()}>
        <Image
          style={styles.image}
          source={image ? { uri: image } : type === 'user' ? defaultProfileImage : defaultClubImage}
          onLoadStart={() => { setLoading(true); }}
          onLoadEnd={() => { setLoading(false); }}
        />
        <View style={styles.imageOverlay} />
        {loading
          ? <ActivityIndicator size="small" color="white" />
          : <Feather name="camera" size={40} color="white" />}
      </TouchableOpacity>
    </View>
  );
};
