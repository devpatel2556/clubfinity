import React from 'react';
import { Avatar, HStack, Text } from 'native-base';
import PropTypes from 'prop-types';

const ClubPostHeader = ({ name, thumbnail }) => (
  <HStack width="100%" paddingTop="1%" paddingBottom="1%" alignItems="center">
    <Avatar size="sm" source={{ uri: thumbnail }} />
    <Text marginLeft="2%" fontSize="md" bold>
      {name}
    </Text>
  </HStack>
);

ClubPostHeader.propTypes = {
  name: PropTypes.string.isRequired,
  thumbnail: PropTypes.string.isRequired,
};

export default ClubPostHeader;
