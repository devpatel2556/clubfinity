import React, { Component } from 'react';
import {
  Image, StyleSheet, Text, TouchableOpacity, View,
} from 'react-native';
import { Select } from 'native-base';
import PropTypes from 'prop-types';
import { Ionicons } from '@expo/vector-icons';
import RenderHTML from 'react-native-render-html';
import { primary } from '../assets/styles/stylesheet';
import SECIcon from '../assets/images/sec-icon.png';
import colors from '../util/colors';
import standardColors from '../constants/Colors';
import EventsApi from '../api/EventsApi';
import Layout from '../constants/Layout';
import { formatToTime, hoursUntil, daysUntil } from '../util/dateUtil';
import {
  cancelNotification,
  scheduleNotification,
} from '../util/localNotifications';

const style = StyleSheet.create({
  container: {
    backgroundColor: standardColors.mutedWhite,
    padding: 15,
    marginRight: 5,
    marginLeft: 5,
    marginTop: 5,
    marginBottom: 5,
    borderColor: standardColors.white,
    borderRadius: 5,
    borderWidth: 4,
    elevation: 2,
  },
  banner_container: {
    flexDirection: 'row',
  },
  banner: {
    flexDirection: 'column',
    flex: 1,
    marginBottom: 5,
    marginRight: 5,
  },
  bannerTopRow: {
    flexDirection: 'row',
    display: 'flex',
  },
  bannerIcon: {
    resizeMode: 'center',
    borderRadius: 25,
    marginRight: 10,
    height: 21,
    width: 21,
    display: 'flex',
  },
  iconButton: {
    height: 21,
  },
  eventName: {
    color: standardColors.black,
    fontSize: 18,
    fontWeight: 'bold',
    width: '100%',
    marginTop: 9,
  },
  clubNameText: {
    color: standardColors.black,
    flexGrow: 1,
  },
  dateText: {
    color: standardColors.black,
    flex: 1,
    textAlign: 'right',
    fontSize: 18,
  },
  location_container: {
    marginBottom: '2%',
  },
  locationText: {
    fontWeight: '700',
  },
  subtext_container: {
    flexDirection: 'column',
    alignItems: 'flex-end',
  },
  subtext: {
    color: standardColors.gray,
    fontSize: 14,
    marginRight: '0%',
  },
  bodyText: {
    color: standardColors.black,
    fontSize: primary.bodyText.fontSize,
    marginLeft: '2%',
    marginTop: 5,
  },
  responseButtons: {
    flexDirection: 'row',
    marginTop: 10,
    justifyContent: 'space-evenly',
  },
  defaultDateTimeColor: {
    color: standardColors.gray,
  },
  upcomingDateColor: {
    color: standardColors.green,
  },
  upcomingTimeColor: {
    color: standardColors.red,
  },
  statusPickerWrapper: {
    borderRadius: 5,
    marginTop: 1,
    width: '100%',
  },
  statusPicker: {
    width: '100%',
  },
  statusPickerIcon: {
    marginRight: '5%',
  },
  grayPicker: {
    backgroundColor: standardColors.grayFade,
  },
  bluePicker: {
    backgroundColor: standardColors.blueFade,
  },
});

// Maximum length allowed for description
const MAX_DESCRIPTION_LENGTH = 92;

export default class EventCard extends Component {
  static propTypes = {
    userId: PropTypes.string.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    navigation: PropTypes.object.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    event: PropTypes.object.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    updateStatus: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.onFocus();
  }

  onFocus = async () => {
    const { event, userId } = this.props;
    const { uninterestedUsers, goingUsers, interestedUsers } = event;
    this.state = {
      uninterested: uninterestedUsers.includes(userId),
      going: goingUsers.includes(userId),
      interested: interestedUsers.includes(userId),
      // There needs to be a single variable that the picker can read
      goingStatus: this.getStatus(
        uninterestedUsers,
        goingUsers,
        interestedUsers,
        userId,
      ),
    };
  };

  getStatus = (uninterestedUsers, goingUsers, interestedUsers, userId) => {
    if (uninterestedUsers.includes(userId)) {
      return 'Uninterested';
    }
    if (goingUsers.includes(userId)) {
      return 'Going';
    }
    if (interestedUsers.includes(userId)) {
      return 'Interested';
    }
    return 'Undecided';
  };

  eventStatusUpdate = async () => {
    const { updateStatus } = this.props;
    updateStatus();
  };

  uninterestedHandler = async () => {
    const { uninterested } = this.state;
    this.setState({
      uninterested: true,
    });
    const { event, userId } = this.props;
    const { _id } = event;
    if (uninterested) {
      await EventsApi.removeUninterestedUser(_id);
    } else {
      this.setState({
        going: false,
        interested: false,
        goingStatus: 'Uninterested',
      });
      await EventsApi.addUninterestedUser(_id);
      await cancelNotification(_id, userId);
      await this.eventStatusUpdate();
    }
  };

  goingHandler = async () => {
    const { going } = this.state;
    const { event, userId } = this.props;
    const { name, date, _id } = event;
    this.setState({
      going: true,
    });
    if (going) {
      await EventsApi.removeGoingUser(_id, userId);
      await cancelNotification(_id, userId);
    } else {
      this.setState({
        uninterested: false,
        interested: false,
        goingStatus: 'Going',
      });
      await EventsApi.addGoingUser(_id);
      await scheduleNotification(name, date, _id, userId);
      await this.eventStatusUpdate();
    }
  };

  interestedHandler = async () => {
    const { interested } = this.state;
    const { event, userId } = this.props;
    const { name, date, _id } = event;

    this.setState({
      interested: true,
    });
    if (interested) {
      await EventsApi.removeInterestedUser(_id, userId);
      await cancelNotification(_id, userId);
    } else {
      this.setState({
        uninterested: false,
        going: false,
        goingStatus: 'Interested',
      });
      await EventsApi.addInterestedUser(_id);
      await scheduleNotification(name, date, _id, userId);
      await this.eventStatusUpdate();
    }
  };

  undecicedHandler = async () => {
    const { going, interested, uninterested } = this.state;
    const { event, userId } = this.props;
    const { _id } = event;
    if (going) {
      await EventsApi.removeGoingUser(_id, userId);
    }
    if (interested) {
      await EventsApi.removeInterestedUser(_id, userId);
    }
    if (uninterested) {
      await EventsApi.removeUninterestedUser(_id, userId);
    }

    this.setState({
      uninterested: false,
      interested: false,
      going: false,
      goingStatus: 'Undecided',
    });

    await this.eventStatusUpdate();
  };

  handlePickerChange = (value) => {
    if (value === 'Going') {
      this.goingHandler();
    } else if (value === 'Interested') {
      this.interestedHandler();
    } else if (value === 'Uninterested') {
      this.uninterestedHandler();
    } else {
      this.undecicedHandler();
    }
  };

  render() {
    this.onFocus();
    const {
      event, navigation, userId,
    } = this.props;
    const {
      name, location, club, date, description,
    } = event;
    const { goingStatus } = this.state;
    const isAdmin = club.admins.map((admin) => admin._id).includes(userId);

    let descriptionString = description;
    if (description.length > MAX_DESCRIPTION_LENGTH) {
      descriptionString = `${description.substring(
        0,
        MAX_DESCRIPTION_LENGTH,
      )}...`;
    }

    let liveDateStyle = style.defaultDateTimeColor;
    let liveTimeStyle = style.defaultDateTimeColor;

    const hoursUntilEvent = hoursUntil(date);
    const daysUntilEvent = daysUntil(date);

    if (daysUntilEvent <= 1) {
      liveDateStyle = style.upcomingDateColor;
    }
    if (daysUntilEvent <= 1 && hoursUntilEvent < 2) {
      liveTimeStyle = style.upcomingTimeColor;
    }

    const getPickerColor = () => {
      if (
        goingStatus === 'Going'
        || goingStatus === 'Interested'
        || goingStatus === 'Uninterested'
      ) {
        return style.bluePicker.backgroundColor;
      }
      return style.grayPicker.backgroundColor;
    };

    return (
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('Event', {
            event,
            updateStatus: this.eventStatusUpdate,
            isAdmin,
          });
        }}
        style={style.container}
      >
        <View style={style.banner_container}>
          <View style={style.banner}>
            <View style={style.bannerTopRow}>
              <View style={{ flexGrow: 0 }}>
                <Image style={style.bannerIcon} source={SECIcon} />
              </View>
              <Text style={style.clubNameText}>{club.name}</Text>
            </View>
            <Text style={style.eventName}>{name}</Text>
          </View>
          <View style={style.subtext_container}>
            <Text style={style.subtext}>
              <Text style={liveDateStyle}>
                {date.monthShort}
                <Text> </Text>
                {date.day}
              </Text>
            </Text>
            <Text style={liveTimeStyle}>
              <Text>{formatToTime(date)}</Text>
            </Text>
          </View>
        </View>
        <RenderHTML source={{ html: descriptionString }} contentWidth={Layout.window.width} />
        {/* <Text style={style.subtext}>{descriptionString}</Text> */}
        {/* to delete */}
        <Text style={[style.subtext, style.location_container]}>
          <Text style={{ textDecorationLine: 'underline' }}>{location}</Text>
        </Text>
        <View
          style={[
            style.statusPickerWrapper,
            { backgroundColor: getPickerColor() },
          ]}
        >
          <Select
            placeholder="Select an option"
            selectedValue={goingStatus}
            onValueChange={this.handlePickerChange}
            width="100%"
            iosIcon={(
              <Ionicons
                name="chevron-down-outline"
                size={20}
                color={colors.inputPlaceholder}
                style={style.statusPickerIcon}
              />
            )}
          >
            <Select.Item
              style={style.pickerItem}
              value="Undecided"
              label="Select an option"
              key="Undecided"
            />
            <Select.Item
              style={style.pickerItem}
              value="Going"
              label="Going"
              key="Going"
            />
            <Select.Item
              style={style.pickerItem}
              value="Interested"
              label="Interested"
              key="Interested"
            />
            <Select.Item
              style={style.pickerItem}
              value="Uninterested"
              label="Not Going"
              key="Uninterested"
            />
          </Select>
        </View>
      </TouchableOpacity>
    );
  }
}
