import React from 'react';
import PropTypes from 'prop-types';
import { RefreshControl } from 'react-native';
import { DateTime } from 'luxon';
import ClubsApi from '../api/ClubsApi';
import EventsApi from '../api/EventsApi';
import AnnouncementsApi from '../api/AnnouncementsApi';

export default class CustomRefresh extends React.Component {
  static propTypes = {
    changeHandler: PropTypes.func.isRequired,
    reqs: PropTypes.shape({
      screen: PropTypes.string,
      clubId: PropTypes.string,
      clubs: PropTypes.array,
      user: PropTypes.instanceOf(Object),
      club: PropTypes.instanceOf(Object),
    }).isRequired,
    style: PropTypes.instanceOf(Object),
    children: PropTypes.oneOfType([
      PropTypes.element,
      PropTypes.arrayOf(PropTypes.element),
    ]),
  };

  static defaultProps = {
    style: null,
    children: null,
  };

  constructor(props) {
    super(props);
    this.onRefresh = this.onRefresh.bind(this);
    this.screensRefresh = {
      ClubScr: this.clubscrRefresh,
      DiscoverScr: this.discoverscrRefresh,
      HomeScr: this.homescrRefresh,
      AnnouncementList: this.announcementListRefresh,
      EventList: this.eventListRefresh,
    };
    this.state = {
      refreshing: false,
    };
  }

  homescrRefresh = async () => {
    const { reqs } = this.props;
    const { clubs } = reqs;
    const homescrState = {
      isFollowingClubs: true,
      events: [],
    };
    if (clubs.length === 0) {
      homescrState.events = [];
      homescrState.isFollowingClubs = false;
    } else {
      homescrState.events = await EventsApi.getFollowing();
    }
    return homescrState;
  }

  discoverscrRefresh = async () => {
    const discoverscrState = {
      clubs: '',
      filteredClubs: '',
    };
    discoverscrState.clubs = await ClubsApi.getAllClubs();
    discoverscrState.clubs = discoverscrState.clubs
      .map((value) => ({ value, sort: Math.random() }))
      .sort((a, b) => a.sort - b.sort)
      .map(({ value }) => value);
    discoverscrState.filteredClubs = discoverscrState.clubs;
    return discoverscrState;
  }

  clubscrRefresh = async () => {
    const { reqs } = this.props;
    const { club, user } = reqs;
    let clubscrState = {
      isAdmin: false,
      announcements: [],
      events: [],
    };
    if (club.admins.map((admin) => admin._id).includes(user._id)) {
      clubscrState = { ...clubscrState, isAdmin: true };
    }
    const { events, announcements } = await ClubsApi.getPosts(club._id);
    const today = Date.parse(new Date());
    const upcomingEvents = events.filter((event) => Date.parse(event.date) >= today);
    clubscrState = { ...clubscrState, events: upcomingEvents, announcements };
    return clubscrState;
  }

  announcementListRefresh = async () => {
    const { reqs } = this.props;
    const { clubId } = reqs;
    let announcementsState = {
      announcements: [],
    };
    const announcements = await AnnouncementsApi.getForClub(
      clubId,
    );
    announcementsState = { ...announcements };
    return announcementsState;
  }

  eventListRefresh = async () => {
    const { reqs } = this.props;
    const { clubId } = reqs;
    let eventListState = {
      pastEvents: [],
      upcomingEvents: [],
    };
    const events = await EventsApi.getForClub(clubId);
    const now = DateTime.local();
    const pastEvents = events.filter((event) => event.date < now);
    const upcomingEvents = events.filter((event) => event.date > now);

    eventListState = { pastEvents, upcomingEvents };
    return eventListState;
  }

  inputChange = async () => {
    const { changeHandler, reqs } = this.props;
    const { screen } = reqs;
    const newState = await this.screensRefresh[screen]();
    changeHandler(newState);
  }

  onRefresh = () => {
    this.setState({ refreshing: true });
    this.inputChange().then(() => this.setState({ refreshing: false }));
  }

  render() {
    const {
      changeHandler, reqs, style, children,
    } = this.props;
    const { refreshing } = this.state;
    return (
      <RefreshControl
        changeHandler={changeHandler}
        reqs={reqs}
        style={style}
        children={children}
        onRefresh={this.onRefresh}
        refreshing={refreshing}
      />
    );
  }
}
