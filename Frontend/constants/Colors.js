const tintColor = '#2f95dc';

export default {
  primaryColor: '#7e947f',
  tintColor,
  tabIconDefault: '#ccc',
  tabIconSelected: tintColor,
  tabBar: '#fefefe',
  errorBackground: 'red',
  errorText: '#fff',
  warningBackground: '#EAEB5E',
  warningText: '#d35400',
  noticeBackground: tintColor,
  noticeText: '#fff',
  // new design system -> https://www.figma.com/file/78djppdiXCcaBFj0zpOJ0d/Design-System?node-id=33%3A1122
  blue: '#006AFF',
  red: '#FF004D',
  green: '#00C05E',
  yellow: '#FFE500',
  white: '#FFFFFF',
  mutedWhite: '#F4F4F4',
  gray: '#8E8E92',
  black: '#000000',
  blueFade: '#BFDAFF',
  redFade: '#FFB8CD',
  greenFade: '#BFDAFF',
  yellowFade: '#BFDAFF',
  grayFade: '#EEEEEE',
};
