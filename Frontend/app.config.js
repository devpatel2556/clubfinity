require('dotenv').config();

export default {
  name: 'Clubfinity',
  description: 'Platform for Gator Club events.',
  slug: 'Clubfinity',
  privacy: 'public',
  platforms: [
    'ios',
    'android',
  ],
  version: '1.0.28',
  orientation: 'portrait',
  icon: './assets/icon.png',
  splash: {
    image: './assets/splash.png',
    resizeMode: 'contain',
    backgroundColor: '#ffffff',
  },
  updates: {
    fallbackToCacheTimeout: 0,
  },
  assetBundlePatterns: [
    '**/*',
  ],
  android: {
    package: 'com.ufsec.Clubfinity',
    googleServicesFile: './google-services.json',
    useNextNotificationsApi: true,
    permissions: [],
    versionCode: 19,
  },
  ios: {
    supportsTablet: true,
    bundleIdentifier: 'com.pvanderlaat.clubfinity',
  },
  extra: {
    API_URL: process.env.API_URL,
  },
};
