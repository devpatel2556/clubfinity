import React, { useState, useEffect } from 'react';
import {
  Platform,
  StatusBar,
  LogBox,
} from 'react-native';
import { registerRootComponent } from 'expo';
import { NativeBaseProvider } from 'native-base';
import * as Font from 'expo-font';
// eslint-disable-next-line camelcase
import { useFonts, Roboto_100Thin, Roboto_500Medium } from '@expo-google-fonts/roboto';
import { Asset } from 'expo-asset';
import * as SplashScreen from 'expo-splash-screen';
import SplashImage from './assets/splash.png';
import UserContext from './util/UserContext';
import AppNavigator from './navigation/AppNavigator';
// import Snackbar from './components/Snackbar';

LogBox.ignoreAllLogs(true);
SplashScreen.preventAutoHideAsync();

export default function App() {
  const [isLoadingComplete, setIsLoadingComplete] = useState(false);
  const [user, setUser] = useState(null);
  const [message, setMessage] = useState('');

  const cacheResources = async () => {
    const images = [SplashImage];
    const cacheImages = images.map((image) => Asset.fromModule(image).downloadAsync());
    return Promise.all(cacheImages);
  };

  useEffect(() => {
    async function prepare() {
      await Font.loadAsync({
        Roboto_100Thin,
        Roboto_500Medium,
      });
      await cacheResources();
      setIsLoadingComplete(true);
    }
    prepare();
  }, []);

  const [fontsLoaded] = useFonts({
    Roboto_100Thin,
    Roboto_500Medium,
  });

  if (!isLoadingComplete || !fontsLoaded) {
    return null;
  }

  return (
    <NativeBaseProvider>
      {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
      <UserContext.Provider
        value={{
          user,
          setUser,
          message,
          setMessage,
        }}
      >
        <AppNavigator />
        {/* <Snackbar /> */}
      </UserContext.Provider>
    </NativeBaseProvider>
  );
}

registerRootComponent(App);
