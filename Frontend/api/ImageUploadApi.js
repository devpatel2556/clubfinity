import * as SecureStore from 'expo-secure-store';
import API from './BaseApi';

exports.upload = async (type, imageUri) => {
  const bearerToken = await SecureStore.getItemAsync('userToken');
  const data = new FormData();
  data.append('image', {
    uri: imageUri,
    type: 'image/jpeg',
    name: 'image.jpg',
  });
  try {
    const axiosResponse = await API.post(`/api/images/${type}`, data, {
      headers: {
        Authorization: `Bearer ${bearerToken}`,
        'Content-Type': 'multipart/form-data',
      },
    })
      .then(async (response) => (response.data))
      .catch((error) => {
        console.error(error);
        return false;
      });
    return axiosResponse.data;
  } catch (error) {
    console.error(error);
    return false;
  }
};
