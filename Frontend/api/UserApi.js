import * as SecureStore from 'expo-secure-store';
import API from './BaseApi';

exports.registerNewUser = async (userData) => API.post('/api/users/register', userData)
  .then((response) => response.data)
  .catch((error) => error.response.data || { ok: false, error: error.message });

exports.resendEmailVerificationCode = async (userId) => API.post('/api/users/resend', { userId })
  .then((response) => response.data);

exports.verifyEmailCode = async (userId, code) => API.post('/api/users/verify', { userId, code })
  .then((response) => response.data)
  .catch((error) => error.response.data || { ok: false, error: error.message });

exports.getUser = async () => {
  const bearerToken = await SecureStore.getItemAsync('userToken');

  const axiosResponse = await API.get('/api/users/', {
    headers: {
      Authorization: `Bearer ${bearerToken}`,
    },
  })
    .then(async (response) => response)
    .catch((error) => {
      if (error.message) {
        return { error };
      }
      return { error: 'Unable to get user' };
    });

  return axiosResponse;
};
exports.forgotPasswordEmail = async (email) => API.post('/api/users/forgot-password', { email })
  .then((response) => response.data)
  .catch((error) => error.response.data || { ok: false, error: error.message });

exports.resetPassword = async (code, newPass, userId) => API.post('/api/users/reset-password',
  { code, newPass, userId })
  .then((response) => response.data)
  .catch((error) => error.response.data || { ok: false, error: error.message });

exports.updateUser = async (user) => {
  const bearerToken = await SecureStore.getItemAsync('userToken');
  const axiosResponse = await API.put(
    '/api/users/',
    user,
    {
      headers: {
        Authorization: `Bearer ${bearerToken}`,
      },
    },
  )
    .then(async (response) => response.data)
    .catch((error) => {
      if (error) {
        return error;
      }
      return { error: 'Unable to update profile' };
    });
  return axiosResponse;
};

exports.updateSettings = async (user) => {
  const bearerToken = await SecureStore.getItemAsync('userToken');
  const axiosResponse = await API.patch(
    '/api/users/user-settings/'
    + `?announcementNotifications=${user.settings.announcementNotifications}`
    + `&eventNotifications=${user.settings.eventNotifications}`
    + `&eventReminderNotifications=${user.settings.eventReminderNotifications}`,
    user,
    {
      headers: {
        Authorization: `Bearer ${bearerToken}`,
      },
    },
  )
    .then(async (response) => response.data)
    .catch((error) => {
      if (error) {
        return error;
      }
      return { error: 'Unable to update settings' };
    });
  return axiosResponse;
};

exports.updateClub = async (clubId, follow) => {
  const bearerToken = await SecureStore.getItemAsync('userToken');
  const axiosResponse = await API.patch(
    `/api/users/clubs/${clubId}?follow=${follow}`,
    {},
    {
      headers: {
        Authorization: `Bearer ${bearerToken}`,
      },
    },
  )
    .then(async (response) => response)
    .catch((error) => {
      if (error) {
        return error;
      }
      return { error: 'Unable to follow club' };
    });
  return axiosResponse;
};

exports.updatePushToken = async (pushToken) => {
  const bearerToken = await SecureStore.getItemAsync('userToken');
  const axiosResponse = await API.patch(
    `/api/users/push-token?pushToken=${pushToken}`,
    {},
    {
      headers: {
        Authorization: `Bearer ${bearerToken}`,
      },
    },
  )
    .then(async (response) => response)
    .catch((error) => {
      if (error) {
        return error;
      }
      return { error: 'Unable to update push token' };
    });
  return axiosResponse;
};

exports.followClub = async (clubId) => exports.updateClub(clubId, true);

exports.unfollowClub = async (clubId) => exports.updateClub(clubId, false);

exports.deleteUser = async (userId) => {
  const bearerToken = await SecureStore.getItemAsync('userToken');
  return API.delete(`/api/users/${userId}`, {
    headers: {
      Authorization: `Bearer ${bearerToken}`,
    },
  });
};
