import * as SecureStore from 'expo-secure-store';
import API from './BaseApi';

exports.getAllClubs = async () => {
  const bearerToken = await SecureStore.getItemAsync('userToken');
  const resp = await API.get('/api/clubs?type=all', {
    headers: {
      Authorization: `Bearer ${bearerToken}`,
    },
  });
  return resp.data.data;
};

exports.getManaging = async () => {
  const bearerToken = await SecureStore.getItemAsync('userToken');
  const resp = await API.get('/api/clubs?type=fromAdminId', {
    headers: {
      Authorization: `Bearer ${bearerToken}`,
    },
  });

  return resp.data.data;
};

exports.getAdmins = async (clubId) => {
  const bearerToken = await SecureStore.getItemAsync('userToken');
  const resp = await API.get(`/api/clubs/${clubId}?select=all`, {
    headers: {
      Authorization: `Bearer ${bearerToken}`,
    },
  });
  return resp.data.data.admins;
};

exports.addAdmin = async (clubId, email) => {
  const bearerToken = await SecureStore.getItemAsync('userToken');
  try {
    const resp = await API.put(`/api/clubs/${clubId}/add-admin`, { email }, {
      headers: {
        Authorization: `Bearer ${bearerToken}`,
      },
    });
    // console.log(resp);
    return resp.data.data;
  } catch (err) {
    console.log(err.response.data);
    return err.response.data;
  }
};

exports.getGoogleCalID = async (clubId) => {
  const bearerToken = await SecureStore.getItemAsync('userToken');
  const resp = await API.get(`/api/clubs/${clubId}?select=all`, {
    headers: {
      Authorization: `Bearer ${bearerToken}`,
    },
  });
  return resp.data.data.googleCalendarId;
};

exports.getPosts = async (clubId) => {
  const bearerToken = await SecureStore.getItemAsync('userToken');
  const resp = await API.get(`/api/clubs/${clubId}?select=posts`, {
    headers: {
      Authorization: `Bearer ${bearerToken}`,
    },
  });
  return resp.data.data;
};

exports.createClub = async (
  clubName,
  clubCategory,
  clubDescription,
  tags,
  thumbnailUrl,
  facebookLink,
  instagramLink,
  slackLink,
) => {
  const newClubData = {
    name: clubName,
    category: clubCategory,
    description: clubDescription,
    tags,
  };
  if (thumbnailUrl) {
    newClubData.thumbnailUrl = thumbnailUrl;
  }
  if (facebookLink) {
    newClubData.facebookLink = facebookLink;
  }
  if (instagramLink) {
    newClubData.instagramLink = instagramLink;
  }
  if (slackLink) {
    newClubData.slackLink = slackLink;
  }
  try {
    const bearerToken = await SecureStore.getItemAsync('userToken');
    const resp = await API.post('/api/clubs/', newClubData, {
      headers: {
        Authorization: `Bearer ${bearerToken}`,
      },
    });
    return resp.data.ok
      ? { successfulRequest: true, data: resp.data.data }
      : { successfulRequest: false, error: resp.data.error };
  } catch (error) {
    return { successfulRequest: false, error };
  }
};

exports.updateClub = async (clubID, club) => {
  try {
    const bearerToken = await SecureStore.getItemAsync('userToken');
    const resp = await API.put(`/api/clubs/${clubID}`, club, {
      headers: {
        Authorization: `Bearer ${bearerToken}`,
      },
    });
    return { successfulRequest: true, data: resp.data.data };
  } catch (error) {
    return { successfulRequest: false, error };
  }
};

exports.googleCalService = async (id, googleCalAccount) => {
  try {
    const reqBody = {
      googleCal: googleCalAccount,
    };
    const bearerToken = await SecureStore.getItemAsync('userToken');
    const resp = await API.patch(`/api/clubs/${id}/google-cal-events`, reqBody,
      {
        headers: {
          Authorization: `Bearer ${bearerToken}`,
        },
      });
    return { successfulRequest: resp.data, data: resp.data.data };
  } catch (error) {
    return { successfulRequest: false, error };
  }
};

exports.checkGoogleCalID = async (googleCalAccount) => {
  try {
    const bearerToken = await SecureStore.getItemAsync('userToken');
    const resp = await API.get(`/api/clubs/${googleCalAccount}/google-cal-check`,
      {
        headers: {
          Authorization: `Bearer ${bearerToken}`,
        },
      });
    return { successfulRequest: resp.data, data: resp.data.data };
  } catch (error) {
    return { successfulRequest: false, error };
  }
};

exports.checkClubName = async (clubName) => {
  try {
    const bearerToken = await SecureStore.getItemAsync('userToken');
    const resp = await API.get('/api/clubs/exists',
      {
        params: {
          name: clubName,
        },
        headers: {
          Authorization: `Bearer ${bearerToken}`,
        },
      });
    return { successfulRequest: resp.data, data: resp.data.data };
  } catch (error) {
    return { successfulRequest: false, error };
  }
};
