import React, { Component } from 'react';
import { View } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Form from '../components/Form';
import TicketsApi from '../api/TicketsApi';
import ClubsAPI from '../api/ClubsApi';
import ImageUploadAPI from '../api/ImageUploadApi';
import Validator from '../components/form/validation/Validator';
import { updateStateAndClearErrors } from '../util/formUtil';
import UserContext from '../util/UserContext';

const clubCategories = [
  { label: 'Computer Science', value: 'Computer Science' },
  { label: 'Research', value: 'Research' },
  { label: 'Business', value: 'Business' },
  { label: 'Arts', value: 'Arts' },
  { label: 'Engineering', value: 'Engineering' },
  { label: 'Health', value: 'Health' },
  { label: 'Journalism', value: 'Journalism' },
  { label: 'Liberal Arts', value: 'Liberal Arts' },
  { label: 'Cultural', value: 'Cultural' },
  { label: 'Honor Society', value: 'Honor Society' },
  { label: 'Media', value: 'Media' },
  { label: 'Professional/Career', value: 'Professional/Career' },
  { label: 'Religious/Spiritual', value: 'Religious/Spiritual' },
  { label: 'Sport Clubs', value: 'Sport Clubs' },
  { label: 'Student Government', value: 'Student Government' },
];

const positions = [
  { label: 'President', value: 'President' },
  { label: 'Vice President', value: 'Vice President' },
  { label: 'Treasurer', value: 'Treasurer' },
  { label: 'Other', value: 'Other' },
];

export default class SubmitClubScr extends Component {
  static contextType = UserContext;

  validator = new Validator({
    clubName: [Validator.required(), Validator.minLength(3)],
    clubDescription: [Validator.required(), Validator.maxLength(280)],
    clubCategory: [Validator.required()],
    position: [Validator.required()],
    facebookLink: [Validator.required(), Validator.validFacebookUrl()],
    instagramLink: [Validator.required(), Validator.validInstagramUrl()],
    slackLink: [Validator.required(), Validator.validSlackUrl()],
    thumbnailUrl: [],
    googleCalLink: [],
  });

  constructor(props) {
    super(props);
    this.state = {
      clubName: '',
      clubDescription: '',
      clubCategory: '',
      position: '',
      tags: 'placeholder',
      facebookLink: '',
      instagramLink: '',
      slackLink: '',
      thumbnailUrl: '',
      googleCalLink: '',
      processingRequest: false,
      errors: {},
    };
  }

  submitClub = async () => {
    const {
      clubName,
      clubCategory,
      clubDescription,
      tags,
      facebookLink,
      instagramLink,
      slackLink,
      thumbnailUrl,
      googleCalLink,
    } = this.state;

    const { navigation } = this.props;
    const { setMessage } = this.context;

    const validationResults = this.validator.validate(this.state);

    const checkClubName = await ClubsAPI.checkClubName(clubName);
    if (!checkClubName.data) {
      validationResults.errors.clubName = 'Club name already exists';
      validationResults.valid = false;
    }

    const validGoogleCalID = await ClubsAPI.checkGoogleCalID(googleCalLink);
    if (googleCalLink !== '' && !validGoogleCalID.data) {
      validationResults.errors.googleCalLink = 'Google calendar not publicly accessible';
      validationResults.valid = false;
    }

    if (!validationResults.valid) {
      this.setState({
        errors: validationResults.errors,
      });
      return;
    }

    let uploadedImage;
    if (thumbnailUrl) {
      uploadedImage = await ImageUploadAPI.upload('club', thumbnailUrl);
    }

    this.setState({ processingRequest: true });

    const submitClubResponse = await TicketsApi.submitClub(
      clubName,
      clubCategory,
      clubDescription,
      tags,
      facebookLink,
      instagramLink,
      slackLink,
      uploadedImage,
      googleCalLink,
    );
    if (submitClubResponse) {
      this.setState({ processingRequest: false });
      setMessage('Club submitted.');
      navigation.goBack();
    } else {
      setMessage('Could not submit club.');
      this.setState({ processingRequest: false });
    }
  };

  render() {
    const {
      errors,
      processingRequest,
      position,
      clubName,
      clubCategory,
      clubDescription,
      facebookLink,
      instagramLink,
      slackLink,
      thumbnailUrl,
      googleCalLink,
    } = this.state;

    return (
      <KeyboardAwareScrollView
        resetScrollToCoords={{ x: 0, y: 0 }}
      >
        <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center' }}>
          <Form>
            <Form.ImagePicker
              text="Upload Club Image"
              image={thumbnailUrl}
              type="club"
              onImagePicked={(value) => updateStateAndClearErrors(this, 'thumbnailUrl', value)}
              error={errors.thumbnailUrl}
            />
            <Form.Text
              value={clubName}
              onChange={(value) => updateStateAndClearErrors(this, 'clubName', value)}
              placeholder="Name"
              error={errors.clubName}
            />
            <Form.Select
              value={position}
              onChange={(value) => updateStateAndClearErrors(this, 'position', value)}
              options={positions}
              placeholder="Position"
              error={errors.position}
            />
            <Form.Select
              value={clubCategory}
              onChange={(value) => updateStateAndClearErrors(this, 'clubCategory', value)}
              options={clubCategories}
              placeholder="Category"
              error={errors.clubCategory}
            />
            <Form.Text
              value={facebookLink}
              onChange={(value) => updateStateAndClearErrors(this, 'facebookLink', value)}
              placeholder="Facebook"
              error={errors.facebookLink}
            />
            <Form.Text
              value={instagramLink}
              onChange={(value) => updateStateAndClearErrors(this, 'instagramLink', value)}
              placeholder="Instagram"
              error={errors.instagramLink}
            />
            <Form.Text
              value={slackLink}
              onChange={(value) => updateStateAndClearErrors(this, 'slackLink', value)}
              placeholder="Slack"
              error={errors.slackLink}
            />
            <Form.Text
              value={googleCalLink}
              onChange={(value) => updateStateAndClearErrors(this, 'googleCalLink', value)}
              placeholder="Google Calendar ID to sync events from"
              error={errors.googleCalLink}
            />
            <Form.TextArea
              value={clubDescription}
              onChange={(value) => updateStateAndClearErrors(this, 'clubDescription', value)}
              placeholder="Description"
              error={errors.clubDescription}
            />
            <Form.Button
              text={processingRequest ? 'Submitting...' : 'Submit Club'}
              onPress={this.submitClub}
            />
          </Form>
        </View>
      </KeyboardAwareScrollView>
    );
  }
}
