import React, { Component } from 'react';
import { Alert, Platform, StyleSheet } from 'react-native';
import {
  Center,
  Container,
  KeyboardAvoidingView,
  ScrollView,
} from 'native-base';
import Form from '../components/Form';
import StandardColors from '../constants/Colors';
import { updateStateAndClearErrors } from '../util/formUtil';
import Validator from '../components/form/validation/Validator';
import EventsApi from '../api/EventsApi';
import UserContext from '../util/UserContext';

const styles = StyleSheet.create({
  deleteButton: {
    backgroundColor: StandardColors.redFade,
  },
  updateButton: {
    backgroundColor: StandardColors.blue,
  },
});

export default class EditEvent extends Component {
  static contextType = UserContext;

  validator = new Validator({
    name: [Validator.required(), Validator.minLength(3)],
    location: [Validator.required(), Validator.minLength(3)],
    facebookLink: [Validator.required(), Validator.validFacebookUrl('Must be valid facebook url')],
    description: [Validator.required()],
  });

  constructor(props) {
    super(props);

    this.state = {
      id: '',
      name: '',
      date: null,
      location: '',
      facebookLink: '',
      description: '',
      processingRequest: { status: false, message: '' },
      processingDelete: { status: false, message: '' },
      errors: {},
    };
  }

  componentDidMount() {
    const { route } = this.props;
    const { event } = route.params;
    const {
      _id: id, name, date, location, facebookLink, description,
    } = event;

    this.setState({
      id,
      name,
      date,
      location,
      facebookLink,
      description,
    });
  }

  editEvent = async () => {
    const validationResult = this.validator.validate(this.state);
    const { setMessage } = this.context;

    if (!validationResult.valid) {
      this.setState({
        processingRequest: { status: false, message: '' },
        errors: validationResult.errors,
      });

      return;
    }

    this.setState({
      processingRequest: { status: true, message: 'Updating...' },
    });

    const {
      id, name, date, location, facebookLink, description,
    } = this.state;

    const updateEventResponse = await EventsApi.update(
      id,
      {
        name,
        date,
        location,
        facebookLink,
        description,
      },
    );

    if (updateEventResponse.error) {
      // eslint-disable-next-line no-alert
      alert('Unable to update event');
      // eslint-disable-next-line no-console
      console.log(updateEventResponse.error);

      this.setState({
        processingRequest: { status: false, message: '' },
      });
    } else {
      setMessage('Event updated.');
      const { navigation } = this.props;
      navigation.pop(2);

      this.setState({
        processingRequest: { status: true, message: 'Saved!' },
      });
    }
  };

  deleteConfirmation = () => Alert.alert(
    'Delete Event?',
    'This action cannot be undone.',
    [
      {
        text: 'Cancel',
        style: 'cancel',
      },
      { text: 'OK', onPress: () => this.deleteEvent() },
    ],
    { cancelable: false },
  );

  deleteEvent = async () => {
    this.setState({
      processingDelete: { status: true, message: 'Deleting...' },
    });

    const { id } = this.state;
    const deleteEventResponse = await EventsApi.delete(id);
    if (deleteEventResponse.error) {
      // eslint-disable-next-line no-alert
      alert('Unable to delete event');
      // eslint-disable-next-line no-console
      console.log(deleteEventResponse.error);

      this.setState({
        processingDelete: { status: false, message: '' },
      });

      return;
    }

    this.setState({
      processingDelete: { status: true, message: 'Deleted!' },
    });

    // Pop the edit event screen and the event screen off the stack
    const { navigation } = this.props;
    navigation.pop(2);
  };

  render() {
    const {
      errors,
      name,
      date,
      location,
      facebookLink,
      description,
      processingRequest,
      processingDelete,
    } = this.state;

    return (
      <Center>
        <Container>
          <KeyboardAvoidingView behavior="height">
            <ScrollView>
              <Form>
                <Form.Text
                  value={name}
                  onChange={(value) => updateStateAndClearErrors(this, 'name', value)}
                  placeholder="Event Name"
                  error={errors.name}
                />
                <Form.Date
                  value={date}
                  onChange={(value) => updateStateAndClearErrors(this, 'date', value)}
                />
                <Form.Text
                  value={location}
                  onChange={(value) => updateStateAndClearErrors(this, 'location', value)}
                  placeholder="Location"
                  error={errors.location}
                />
                <Form.Text
                  value={facebookLink}
                  onChange={(value) => updateStateAndClearErrors(this, 'facebookLink', value)}
                  placeholder="Facebook Link"
                  error={errors.facebookLink}
                  autoCapitalize="none"
                  autoCorrect={false}
                  keyboardType={Platform.OS === 'ios' ? 'url' : 'default'}
                />
                <Form.TextArea
                  value={description}
                  onChange={(value) => updateStateAndClearErrors(this, 'description', value)}
                  placeholder="Description"
                  error={errors.description}
                />
                <Form.Button
                  text={processingRequest.status ? processingRequest.message : 'Update'}
                  style={styles.updateButton}
                  onPress={() => this.editEvent()}
                />
                <Form.Button
                  text={processingDelete.status ? processingDelete.message : 'Delete'}
                  style={styles.deleteButton}
                  onPress={this.deleteConfirmation}
                />
              </Form>
            </ScrollView>
          </KeyboardAvoidingView>
        </Container>
      </Center>
    );
  }
}
