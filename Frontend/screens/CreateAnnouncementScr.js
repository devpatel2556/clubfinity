/* eslint-disable linebreak-style */
import React, { Component } from 'react';
import { Center, Container } from 'native-base';
import { DateTime } from 'luxon';
import AnnouncementsApi from '../api/AnnouncementsApi';
import Form from '../components/Form';
import Validator from '../components/form/validation/Validator';
import { updateStateAndClearErrors } from '../util/formUtil';
import UserContext from '../util/UserContext';

export default class CreateAnnouncementScr extends Component {
  static contextType = UserContext;

  validator = new Validator({
    title: [Validator.required()],
    description: [Validator.required()],
  });

  constructor(props) {
    super(props);
    this.state = {
      title: '',
      description: '',
      errors: {},
    };
  }

  handleCreateAnnouncement = async () => {
    const { route, navigation } = this.props;
    const { title, description } = this.state;
    const { club } = route.params;

    const { setMessage } = this.context;

    const validationResults = this.validator.validate(this.state);

    if (!validationResults.valid) {
      this.setState({ errors: validationResults.errors });

      return;
    }

    await AnnouncementsApi.create({
      title,
      description,
      date: DateTime.local(),
      club: club._id,
    });
    // Handle possible error from api.
    setMessage('Announcement created.');
    navigation.pop();
  };

  render() {
    const { title, description, errors } = this.state;

    return (
      <Center>
        <Container>
          <Form>
            <Form.Text
              placeholder="Title"
              value={title}
              onChange={(value) => updateStateAndClearErrors(this, 'title', value)}
              error={errors.title}
            />
            <Form.TextArea
              placeholder="Description"
              value={description}
              onChange={(value) => updateStateAndClearErrors(this, 'description', value)}
              error={errors.description}
            />
            <Form.Button
              text="Submit"
              onPress={this.handleCreateAnnouncement}
            />
          </Form>
        </Container>
      </Center>
    );
  }
}
