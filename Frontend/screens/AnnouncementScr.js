/* eslint-disable linebreak-style */
import React, { Component } from 'react';
import { DateTime } from 'luxon';
import {
  Button,
  Text,
  Center,
  Container,
  ScrollView,
  VStack,
  Box,
} from 'native-base';
import RenderHTML from 'react-native-render-html';
import Layout from '../constants/Layout';
import colors from '../util/colors';
import UserContext from '../util/UserContext';
import ClubPostHeader from '../components/ClubPostHeader';

const tagsStyles = {
  body: {
    fontSize: '1.25rem',
  },
};

export default class AnnouncementScr extends Component {
  static contextType = UserContext;

  render() {
    const { route, navigation } = this.props;
    const { announcement, isAdmin } = route.params;
    const { title, description, _id: id } = announcement;
    const date = DateTime.fromISO(announcement.date).toFormat('MMM dd yyyy');
    return (
      <ScrollView>
        <Center>
          <Container style={{ width: '95%', paddingTop: '3%' }}>
            <ClubPostHeader
              width="100%"
              name={announcement.club.name}
              thumbnail={announcement.club.thumbnailUrl}
              navigation={navigation}
            />
            <VStack
              style={{
                width: '100%',
                alignItems: 'center',
                justifyContent: 'center',
                paddingBottom: '5%',
              }}
            >
              <VStack
                style={{
                  borderBottomWidth: 0.9,
                  borderBottomColor: colors.grayScale4,
                  width: '100%',
                  marginTop: '3%',
                }}
              >
                <Text bold fontSize="xl" paddingBottom="1%">
                  {title}
                </Text>
                <Text paddingBottom="2%" alignSelf="flex-end">
                  {date}
                </Text>
              </VStack>
              <Box
                style={{
                  width: '100%',
                  margin: '4%',
                  padding: '4%',
                  borderWidth: 0.5,
                  borderColor: 'lightgray',
                }}
              >
                <RenderHTML
                  source={{ html: description }}
                  tagsStyles={tagsStyles}
                  contentWidth={Layout.window.width}
                />
              </Box>
            </VStack>
            {isAdmin ? (
              <Button
                style={{
                  alignSelf: 'center',
                  backgroundColor: colors.secondary0,
                  width: '100%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginBottom: '5%',
                  marginTop: '5%',
                }}
                onPress={() => navigation.navigate('Edit Announcement', { id, title, description })}
              >
                <Text style={{ alignSelf: 'center' }}>Edit</Text>
              </Button>
            ) : null}
          </Container>
        </Center>
      </ScrollView>
    );
  }
}
