import {
  React,
  useState,
  useEffect,
  useContext,
  useCallback,
  useRef,
} from 'react';
import {
  Image,
  View,
  StyleSheet,
  ActivityIndicator,
  Animated,
} from 'react-native';
import { Asset } from 'expo-asset';
import * as SplashScreen from 'expo-splash-screen';
import SplashImage from '../assets/splash.png';
import UserContext from '../util/UserContext';
import UserApi from '../api/UserApi';
import Layout from '../constants/Layout';

export default function AuthScr({ navigation }) {
  const { setUser } = useContext(UserContext);
  const [isLoadingComplete, setIsLoadingComplete] = useState(false);
  const [ready, setReady] = useState(false);
  const animatedValue = useRef(new Animated.Value(0)).current;

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
    },
    background: {
      position: 'absolute',
      width: '100%',
      height: '100%',
    },
    backgroundImg: {
      flex: 1,
      flexDirection: 'column',
      width: '100%',
      height: '100%',
    },
    overlay: {
      flex: 1,
      justifyContent: 'flex-end',
      marginBottom: '55%',
    },
  });

  const cacheResources = async () => {
    const images = [SplashImage];
    const cacheImages = images.map((image) => Asset.fromModule(image).downloadAsync());
    return Promise.all(cacheImages);
  };

  useEffect(() => {
    const loadResources = async () => {
      await cacheResources();
      setIsLoadingComplete(true);
    };
    loadResources();
  }, []);

  const onLayoutRootView = useCallback(async () => {
    if (isLoadingComplete) {
      await SplashScreen.hideAsync();
    }
  }, [isLoadingComplete]);

  const authRouter = async () => {
    let routeName = 'AuthStack';
    const authResponse = await UserApi.getUser();
    if (!authResponse.status && authResponse.error.message !== 'Request failed with status code 401') {
      alert('Couldn\'t connect to Clubfinity servers!');
    }
    if (!authResponse.error && authResponse.data && authResponse.data.data) {
      setUser(authResponse.data.data);
      routeName = 'AppStack';
    }
    if (routeName === 'AuthStack') {
      setReady(true);
      await new Promise((resolve) => setTimeout(resolve, 800));
    }
    navigation.navigate(routeName);
  };

  useEffect(() => {
    authRouter();
  }, []);

  useEffect(() => {
    const endPos = -1 * (Layout.window.height * 0.38) + 130;
    if (ready) {
      Animated.timing(animatedValue, {
        toValue: endPos,
        duration: 800,
        useNativeDriver: true,
      }).start();
    }
  }, [ready]);

  if (ready) {
    return (
      <View style={{ flex: 1 }}>
        <Animated.Image
          source={SplashImage}
          resizeMode="contain"
          style={[
            styles.backgroundImg,
            {
              transform: [{ translateY: animatedValue }],
            },
          ]}
          onLayout={onLayoutRootView}
        />
      </View>
    );
  }

  return (
    <View onLayout={onLayoutRootView} style={styles.container}>
      <View style={styles.background}>
        <Image source={SplashImage} resizeMode="contain" style={styles.backgroundImg} />
      </View>
      <View style={styles.overlay}>
        <ActivityIndicator size="large" color="#006aff" />
      </View>
    </View>
  );
}
