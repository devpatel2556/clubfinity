import React from 'react';
import { Image, StyleSheet, View } from 'react-native';
import { Text } from 'native-base';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import UserApi from '../api/UserApi';
import colors from '../util/colors';
import Majors from '../data/Majors';
import ClassYears from '../data/ClassYears';
import UserContext from '../util/UserContext';
import ClubfinityLogo from '../assets/images/ClubfinityLogo.png';
import Form from '../components/Form';
import Validator from '../components/form/validation/Validator';
import { updateStateAndClearErrors } from '../util/formUtil';

const styles = StyleSheet.create({
  imageContainer: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  image: {
    width: 200,
    height: 200,
    margin: 30,
    marginBottom: 80,
  },
  signInText: {
    alignSelf: 'center',
    opacity: 0.7,
    marginBottom: '5%',
    marginTop: 10,
  },
  signInLink: {
    textDecorationLine: 'underline',
    color: colors.info,
  },
  mainContainer: {
    paddingTop: '12%',
    paddingBottom: '5%',
    paddingHorizontal: 20,
    flex: 1,
    display: 'flex',
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default class SignupScr extends React.Component {
  static navigationOptions = {
    header: null,
  };

  static contextType = UserContext;

  nameRegex = /^[a-z ,.-]+$/i;

  validator = new Validator({
    firstName: [Validator.required(), Validator.regex(this.nameRegex)],
    lastName: [Validator.required(), Validator.regex(this.nameRegex)],
    major: [Validator.required()],
    classYear: [Validator.required(), Validator.numeric()],
    email: [Validator.required(), Validator.email(), Validator.endsWith('@ufl.edu', 'Must be a ufl email')],
    password: [Validator.required(), Validator.minLength(6)],
    verifyPassword: [Validator.required(), Validator.equalsField('password', 'Must equal password')],
  });

  constructor(props) {
    super(props);
    this.state = {
      firstName: '',
      lastName: '',
      major: '',
      classYear: '',
      email: '',
      password: '',
      verifyPassword: '',
      processingRequest: false,
      errors: {},
    };
  }

  redirectToVerificationCodeInput = (userId, email, password) => {
    const { navigation } = this.props;

    navigation.navigate('EmailVerification', {
      userId,
      email,
      password,
    });
  }

  signupHandler = async () => {
    const validationResults = this.validator.validate(this.state);

    if (!validationResults.valid) {
      this.setState({ errors: validationResults.errors });

      return;
    }

    this.setState({ processingRequest: true });

    const {
      firstName,
      lastName,
      password,
      email,
      major,
      classYear,
    } = this.state;

    const registerUserResponse = await UserApi.registerNewUser({
      name: { first: firstName, last: lastName },
      password,
      email,
      major,
      year: classYear,
    });
    this.setState({ processingRequest: false });

    if (!registerUserResponse.ok) {
      alert('Unable to sign up! Please try again later');
      console.log(registerUserResponse.error);
      return;
    }

    console.log(`Successfully created user ${email}`);
    this.redirectToVerificationCodeInput(registerUserResponse.data._id, email, password);
  };

  signIn = async () => {
    const { navigation } = this.props;
    navigation.navigate('SignIn');
  };

  render() {
    const {
      errors,
      processingRequest,
      firstName,
      lastName,
      major,
      classYear,
      email,
      password,
      verifyPassword,
    } = this.state;

    return (
      <KeyboardAwareScrollView
        style={{ flex: 1 }}
      >
        <View style={styles.mainContainer}>
          <View style={styles.imageContainer}>
            <Image
              style={styles.image}
              source={ClubfinityLogo}
            />
          </View>
          <View style={{ flex: 1, width: '100%' }}>
            <Form>
              <Form.Text
                placeholder="Email"
                value={email}
                onChange={(value) => updateStateAndClearErrors(this, 'email', value)}
                error={errors.email}
                keyboardType="email-address"
                autoCapitalize="none"
              />
              <Form.Text
                placeholder="First name"
                value={firstName}
                onChange={(value) => updateStateAndClearErrors(this, 'firstName', value)}
                error={errors.firstName}
              />
              <Form.Text
                placeholder="Last name"
                value={lastName}
                onChange={(value) => updateStateAndClearErrors(this, 'lastName', value)}
                error={errors.lastName}
              />
              <Form.Select
                placeholder="Major"
                options={Majors}
                value={major}
                onChange={(value) => updateStateAndClearErrors(this, 'major', value)}
                error={errors.major}
              />
              <Form.Select
                placeholder="Year"
                options={ClassYears}
                value={classYear}
                onChange={(value) => updateStateAndClearErrors(this, 'classYear', value)}
                error={errors.classYear}
              />
              <Form.Text
                placeholder="Password"
                value={password}
                onChange={(value) => updateStateAndClearErrors(this, 'password', value)}
                error={errors.password}
                secureTextEntry
              />
              <Form.Text
                placeholder="Confirm Password"
                value={verifyPassword}
                onChange={(value) => updateStateAndClearErrors(this, 'verifyPassword', value)}
                error={errors.verifyPassword}
                secureTextEntry
              />
              <Form.Button
                text={processingRequest ? 'Signing Up...' : 'Sign Up'}
                onPress={this.signupHandler}
              />
              <Text
                style={styles.signInText}
              >
                Already have an account?
                {' '}
                <Text
                  style={styles.signInLink}
                  onPress={this.signIn}
                >
                  Sign In
                </Text>
              </Text>
            </Form>
          </View>
        </View>
      </KeyboardAwareScrollView>
    );
  }
}
