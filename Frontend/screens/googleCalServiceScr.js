import React, { Component } from 'react';
import { View, Text } from 'react-native';
import {
  Heading, ScrollView, Avatar,
} from 'native-base';
import getTheme from '../native-base-theme/components';
import thumbnailTheme from '../native-base-theme/components/Thumbnail';
import UserContext from '../util/UserContext';
import ClubsApi from '../api/ClubsApi';
import Form from '../components/Form';
import { updateStateAndClearErrors } from '../util/formUtil';
import defaultClubImage from '../assets/images/DefaultClubImage.png';

export default class AddEvents extends Component {
  static contextType = UserContext;

  constructor(props) {
    super(props);

    this.state = {
      id: '',
      googleCalendarId: '',
      thumbnailUrl: '',
      processingRequest: false,
      errors: {},
    };
  }

  componentDidMount() {
    const { route } = this.props;
    const { club } = route.params;
    this.setState({
      id: club._id,
      thumbnailUrl: club.thumbnailUrl,
      googleCalendarId: club.googleCalendarId,
    });
    this.getUpdatedGoogleId(club._id);
  }

  getUpdatedGoogleId = async (clubID) => {
    const updatedGoogleCalID = await ClubsApi.getGoogleCalID(clubID);
    this.setState({
      googleCalendarId: updatedGoogleCalID,
    });
  }

  editClub = async () => {
    this.setState({ processingRequest: true });
    const { route } = this.props;
    const { club } = route.params;

    const {
      googleCalendarId,
    } = this.state;

    const updatedClubValues = {
      ...club,
      googleCalendarId,
    };
    const editedClubResponse = await ClubsApi.updateClub(
      club._id,
      updatedClubValues,
    );
    if (editedClubResponse.successfulRequest) {
      this.setState({ processingRequest: false });
      // setMessage('Club updated.');
      alert('Club fully updated.');
      // navigation.pop();
    } else {
      // setMessage('Unable to update club.');
      alert('Club failed to update.');
      this.setState({ processingRequest: false });
    }
  };

  attemptUpdateGoogleCal = async () => {
    this.setState({ processingRequest: true });

    const { navigation } = this.props;

    const {
      id,
      googleCalendarId,
    } = this.state;

    const editedClubResponse = await ClubsApi.googleCalService(
      id,
      googleCalendarId,
    );
    if (editedClubResponse.data) {
      this.setState({ processingRequest: false });
      // alert('Club updated.');
      this.editClub();
      navigation.pop();
    } else {
      alert('Invalid calendar id');
      console.log(editedClubResponse.error);
      this.setState({ processingRequest: false });
    }
  };

  render() {
    const {
      thumbnailUrl,
      processingRequest,
      googleCalendarId,
      errors,
    } = this.state;

    return (
      <ScrollView>
        <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center' }}>
          <Form>
            <View style={{ display: 'flex', alignItems: 'center' }}>
              <View style={{ paddingTop: '10%' }}>
                <Avatar
                  source={thumbnailUrl ? { uri: thumbnailUrl } : defaultClubImage}
                  size="lg"
                  style={getTheme(thumbnailTheme)}
                />
              </View>
              <Heading size="xl" style={{ fontSize: 20 }}>
                Sync events with google calendar
              </Heading>
              <Text style={{ fontSize: 12, top: 2, fontWeight: 'bold' }}>
                (Clubfinity will sync up every 24 hours)
              </Text>
            </View>
            <Form.Text
              placeholder="enter the gmail for the google calendar"
              value={googleCalendarId}
              onChange={(value) => updateStateAndClearErrors(this, 'googleCalendarId', value)}
              error={errors.googleCalAccount}
            />
            <Form.Button
              text={processingRequest ? 'Editing...' : 'Save'}
              onPress={this.attemptUpdateGoogleCal}
            />
          </Form>
          <View style={{ display: 'flex', alignItems: 'center' }}>
            {/* <Text style={{ fontSize: 16, top: 2, textDecorationLine: 'underline' }}>
              Steps to find calendar email/id:
            </Text>
            <Text style={{ fontSize: 16, top: 2 }}>
              Open your Google Calendar app page using your Google account.
            </Text>
            <Text style={{ fontSize: 16, top: 2 }}>
              Navigate to your subscribed/available Google calendars list (usually bottom left side).
            </Text>
            <Text style={{ fontSize: 16, top: 2 }}>
              To get to your calendar settings, hover over the calendar you wish to work on
              and click the three vertical dots that appear to the right - this will bring up a dropdown menu,
              click Settings and sharing.
            </Text>
            <Text style={{ fontSize: 16, top: 2 }}>
              A new page will open. Find the Calendar ID at the bottom under the Integrate Calendar section.
            </Text> */}
            <Text style={{ fontSize: 12, top: 2 }}>
              To ensure your calendar will work correctly, go to:
            </Text>
            <Text style={{ fontSize: 12, top: 2, textDecorationLine: 'underline' }}>
              docs.simplecalendar.io/find-google-calendar-id
            </Text>
          </View>
        </View>
      </ScrollView>
    );
  }
}
