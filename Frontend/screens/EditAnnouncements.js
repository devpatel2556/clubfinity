import React, { Component } from 'react';
import {
  Alert, StatusBar, StyleSheet, Text,
} from 'react-native';
import { Button, View, ScrollView } from 'native-base';
import colors from '../util/colors';
import AnnouncementsApi from '../api/AnnouncementsApi';
import Form from '../components/Form';
import Validator from '../components/form/validation/Validator';
import { updateStateAndClearErrors } from '../util/formUtil';
import UserContext from '../util/UserContext';

const STATUS_BAR_HEIGHT = StatusBar.currentHeight;

const styles = StyleSheet.create({
  container: {
    margin: 20,
    marginTop: STATUS_BAR_HEIGHT,
    flex: 1,
    display: 'flex',
    backgroundColor: '#ecf0f1',
    alignItems: 'center',
  },
  headerLeft: {
    marginLeft: 10,
  },
  headerRight: {
    marginRight: 10,
  },
  headerLeftText: {
    color: '#ecf0f1',
  },
  headerRightText: {
    color: '#ecf0f1',
    fontWeight: 'bold',
  },
  SaveButtonStyle: {
    width: '92%',
    alignSelf: 'center',
  },
  deleteButton: {
    backgroundColor: '#ff807f',
    color: 'black',
  },
  buttonText: {
    color: '#ecf0f1',
    fontSize: 18,
  },
});

export default class EditAnnouncements extends Component {
  // Update for later: navigate back to previous screen when pressed
  // TO-DO: Use navigationOptionsBuilder.js

  static contextType = UserContext;

  static navigationOptions = ({ navigation }) => ({
    headerTitle: 'Edit Announcement',
    headerRight: (
      <Button
        onPress={() => {
          navigation.navigate('Club');
        }}
        style={styles.headerRight}
        transparent
      >
        <Text style={styles.headerRightText}>Done</Text>
      </Button>
    ),
    headerLeft: () => (
      <Button
        onPress={() => {
          navigation.navigate('Club');
        }}
        style={styles.headerLeft}
        transparent
      >
        <Text style={styles.headerLeftText}>Cancel</Text>
      </Button>
    ),
    headerStyle: { backgroundColor: colors.primary0 },
    headerTitleStyle: { color: colors.grayScale1, letterSpacing: 2 },
    headerTintColor: 'white',
  });

  validator = new Validator({
    title: [Validator.required()],
    description: [Validator.required()],
  });

  constructor(props) {
    super(props);
    this.state = {
      title: '',
      description: '',
      processingRequest: { status: false, message: '' },
      processingDelete: { status: false, message: 'Deleting...' },
      errors: {},
    };
  }

  componentDidMount() {
    const { route } = this.props;
    const { id, title, description } = route.params;
    this.setState({
      id, title, description,
    });
  }

  editAnnouncement = async () => {
    const validationResult = this.validator.validate(this.state);
    const { setMessage } = this.context;

    if (!validationResult.valid) {
      this.setState({
        processingRequest: { status: false, message: '' },
        errors: validationResult.errors,
      });

      return;
    }

    this.setState({
      processingRequest: { status: true, message: 'Updating...' },
    });

    const { title, description, id } = this.state;
    const updateAnnouncementResponse = await AnnouncementsApi.update(id, {
      title,
      description,
    });

    if (updateAnnouncementResponse.error) {
      alert('Unable to update announcement');
      setMessage('Unable to update announcement.');
      console.log(updateAnnouncementResponse.error);
      return;
    }

    setMessage('Profile updated.');

    this.setState({
      processingRequest: { status: true, message: 'Saved!' },
    });
  };

  deleteAnnouncement = async () => {
    this.setState({
      processingDelete: { status: true, message: 'Deleting...' },
    });
    const { id } = this.state;
    const deleteAnnouncementResponse = await AnnouncementsApi.delete(id);
    if (deleteAnnouncementResponse.error) {
      alert('Unable to delete announcement');
      console.log(deleteAnnouncementResponse.error);
      return;
    }
    this.setState({
      processingDelete: { status: true, message: 'Deleted!' },
    });
    const { navigation } = this.props;
    navigation.navigate('Club');
  };

  deleteConfirmation = () => Alert.alert(
    'Delete Announcement?',
    'This action cannot be undone.',
    [
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      { text: 'Delete', onPress: () => this.deleteAnnouncement() },
    ],
    { cancelable: false },
  );

  createTwoButtonAlert = () => Alert.alert(
    'Alert Title',
    'My Alert Msg',
    [
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      { text: 'OK', onPress: () => console.log('OK Pressed') },
    ],
    { cancelable: false },
  );

  render() {
    const {
      errors, title, description, processingRequest, processingDelete,
    } = this.state;
    return (
      <ScrollView>
        <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center' }}>
          <Form>
            <Form.Text
              label="Title"
              placeholder="Title"
              value={title}
              onChange={(value) => updateStateAndClearErrors(this, 'title', value)}
              error={errors.title}
            />
            <Form.TextArea
              label="Description"
              placeholder="Description"
              value={description}
              onChange={(value) => updateStateAndClearErrors(this, 'description', value)}
              error={errors.description}
            />
            <Form.Button
              text={
                processingRequest.status ? processingRequest.message : 'Update'
              }
              onPress={this.editAnnouncement}
            />
            <Form.Button
              text={
                processingDelete.status ? processingDelete.message : 'Delete'
              }
              style={styles.deleteButton}
              onPress={() => this.deleteConfirmation()}
            />
          </Form>
        </View>
      </ScrollView>
    );
  }
}
