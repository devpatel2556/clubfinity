import React, { Component } from 'react';
import { View } from 'react-native';
import {
  Heading, ScrollView,
} from 'native-base';
import UserContext from '../util/UserContext';
import ClubsApi from '../api/ClubsApi';
import ImageUploadAPI from '../api/ImageUploadApi';
import Form from '../components/Form';
import { updateStateAndClearErrors } from '../util/formUtil';
import Validator from '../components/form/validation/Validator';
import Colors from '../constants/Colors';

export default class EditClub extends Component {
  static contextType = UserContext;

  validator = new Validator({
    name: [Validator.required(), Validator.minLength(3)],
    description: [Validator.required(), Validator.maxLength(280)],
    facebookLink: [Validator.required(), Validator.validFacebookUrl()],
    instagramLink: [Validator.required(), Validator.validInstagramUrl()],
    slackLink: [Validator.required(), Validator.validSlackUrl()],
    thumbnailUrl: [],
  });

  constructor(props) {
    super(props);

    this.state = {
      name: '',
      description: '',
      facebookLink: '',
      instagramLink: '',
      slackLink: '',
      thumbnailUrl: '',
      processingRequest: false,
      errors: {},
    };
  }

  componentDidMount() {
    const { route } = this.props;
    const { club } = route.params;
    this.setState({
      name: club.name,
      description: club.description,
      facebookLink: club.facebookLink,
      instagramLink: club.instagramLink,
      slackLink: club.slackLink,
      thumbnailUrl: club.thumbnailUrl,
      id: club._id,
    });
  }

  editClub = async () => {
    const validationResults = this.validator.validate(this.state);

    if (!validationResults.valid) {
      this.setState({
        processingRequest: false,
        errors: validationResults.errors,
      });

      return;
    }
    this.setState({ processingRequest: true });

    const { route, navigation } = this.props;
    const { club, updateClubState } = route.params;
    const { setMessage } = this.context;

    const {
      name, description, slackLink, facebookLink, instagramLink, thumbnailUrl,
    } = this.state;

    let uploadedImage;
    if (thumbnailUrl !== club.thumbnailUrl) {
      uploadedImage = await ImageUploadAPI.upload('club', thumbnailUrl);
    }

    const updatedClubValues = {
      ...club,
      name,
      description,
      slackLink,
      facebookLink,
      instagramLink,
      thumbnailUrl: uploadedImage !== null ? uploadedImage : club.thumbnailUrl,
    };

    const editedClubResponse = await ClubsApi.updateClub(
      club._id,
      updatedClubValues,
    );
    if (editedClubResponse.successfulRequest) {
      this.setState({ processingRequest: false });
      setMessage('Club updated.');

      updateClubState(editedClubResponse.data);

      navigation.pop();
    } else {
      setMessage('Unable to update club.');
      console.log(editedClubResponse.error);
      this.setState({ processingRequest: false });
    }
  };

  render() {
    const { navigation, route } = this.props;
    const { club } = route.params;
    const {
      name,
      description,
      thumbnailUrl,
      slackLink,
      facebookLink,
      instagramLink,
      errors,
      processingRequest,
    } = this.state;

    return (
      <ScrollView>
        <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center' }}>
          <Form>
            <Form.ImagePicker
              text="Upload Club Image"
              image={thumbnailUrl}
              type="club"
              onImagePicked={(value) => updateStateAndClearErrors(this, 'thumbnailUrl', value)}
              error={errors.thumbnailUrl}
            />
            <View style={{ display: 'flex', alignItems: 'center' }}>
              <Heading
                size="xl"
                style={{ paddingBottom: '2%', paddingTop: '5%', textAlign: 'center' }}
              >
                {name}
              </Heading>
            </View>
            <Form.Text
              placeholder="Facebook"
              value={facebookLink}
              onChange={(value) => updateStateAndClearErrors(this, 'facebookLink', value)}
              error={errors.facebookLink}
            />
            <Form.Text
              placeholder="Instagram"
              value={instagramLink}
              onChange={(value) => updateStateAndClearErrors(this, 'instagramLink', value)}
              error={errors.instagramLink}
            />
            <Form.Text
              placeholder="Slack"
              value={slackLink}
              onChange={(value) => updateStateAndClearErrors(this, 'slackLink', value)}
              error={errors.slackLink}
            />
            <Form.TextArea
              placeholder="Description"
              value={description}
              onChange={(value) => updateStateAndClearErrors(this, 'description', value)}
              error={errors.description}
            />
            <Form.Button
              style={{ backgroundColor: Colors.green }}
              text={processingRequest ? 'Editing...' : 'Sync events with google calendar'}
              onPress={() => navigation.navigate('Google Calendar', {
                club, navigation,
              })}
            />
            <Form.Button
              text={processingRequest ? 'Editing...' : 'Save'}
              onPress={this.editClub}
            />
          </Form>
        </View>
      </ScrollView>
    );
  }
}
