import React, { Component } from 'react';
import { View, SectionList, Text } from 'react-native';
import CustomRefresh from '../components/CustomRefresh';
import AnnouncementsApi from '../api/AnnouncementsApi';
import Row from '../components/Row';

class AnnouncementList extends Component {
  constructor(props) {
    super(props);
    this.isAdmin = false;
    this.state = {
      announcements: [],
      loading: false,
    };
  }

  async componentDidMount() {
    const { route } = this.props;
    const { clubId, isAdmin } = route.params;
    this.setState({ loading: true });
    const announcements = await AnnouncementsApi.getForClub(
      clubId,
    );
    announcements.reverse();
    this.isAdmin = isAdmin;
    this.setState({ announcements, loading: false });
  }

  changeHandler = (newState) => {
    this.setState(newState);
  }

  renderSectionHeader = (section) => (
    <Text
      style={{
        fontWeight: 'bold',
        fontSize: 18,
        marginBottom: 15,
        backgroundColor: 'white',
      }}
    >
      {section.title}
    </Text>
  );

  renderEmpty = () => (
    <Text style={{ color: 'rgba(0, 0, 0, 0.50)', textAlign: 'center' }}>
      No Announcements
    </Text>
  );

  renderLoading = () => (
    <View style={{ flex: 1, paddingVertical: 20, paddingHorizontal: 30 }}>
      <Text style={{ color: 'rgba(0, 0, 0, 0.50)', textAlign: 'center' }}>Loading Announcements...</Text>
    </View>
  )

  render() {
    const { navigation, route } = this.props;
    const { announcements, loading } = this.state;
    const { clubId } = route.params;

    if (loading) return this.renderLoading();

    const listData = [];
    if (announcements.length > 0) {
      listData.push({ title: 'Announcements', data: announcements });
    }

    return (
      <View style={{ flex: 1, paddingVertical: 20, paddingHorizontal: 30 }}>
        <SectionList
          refreshControl={(
            <CustomRefresh
              changeHandler={this.changeHandler}
              reqs={{ screen: 'AnnouncementList', clubId }}
            />
          )}
          sections={listData}
          keyExtractor={(announcement) => announcement._id}
          renderItem={({ item }) => (
            <Row
              date={item.date.toFormat('MMM dd yyyy')}
              text={item.title}
              handler={() => {
                navigation.navigate('Announcement', {
                  announcement: item,
                  isAdmin: this.isAdmin,
                });
              }}
            />
          )}
          renderSectionHeader={({ section }) => this.renderSectionHeader(section)}
          ListEmptyComponent={this.renderEmpty}
        />
      </View>
    );
  }
}

export default AnnouncementList;
