import React, { Component } from 'react';
import { ScrollView } from 'react-native';
import TicketsApi from '../api/TicketsApi';
import Form from '../components/Form';
import Validator from '../components/form/validation/Validator';
import { updateStateAndClearErrors } from '../util/formUtil';

import UserContext from '../util/UserContext';

export default class ReportBugScr extends Component {
  static contextType = UserContext;

  validator = new Validator({
    bugDescription: [Validator.required(), Validator.minLength(10)],
  });

  constructor(props) {
    super(props);
    this.state = {
      bugDescription: '',
      processingRequest: false,
      errors: {},
    };
  }

  submitReport = async () => {
    const validationResults = this.validator.validate(this.state);
    const { setMessage } = this.context;

    if (!validationResults.valid) {
      this.setState({ errors: validationResults.errors });

      return;
    }

    this.setState({ processingRequest: true });

    const { bugDescription } = this.state;
    const { navigation } = this.props;

    const submitBugReportResponse = await TicketsApi.submitBugReport(
      bugDescription,
    );

    if (submitBugReportResponse) {
      setMessage('Bug submitted.');
      this.setState({
        processingRequest: false,
      });
    } else {
      setMessage('Could not submit bug.');
      this.setState({
        processingRequest: false,
      });
    }

    navigation.goBack();
  };

  render() {
    const { bugDescription, errors, processingRequest } = this.state;

    return (
      <ScrollView>
        <Form>
          <Form.Text
            placeholder="What went wrong?"
            value={bugDescription}
            onChange={(value) => updateStateAndClearErrors(this, 'bugDescription', value)}
            error={errors.bugDescription}
          />
          <Form.Button
            text={processingRequest ? 'Submitting...' : 'Submit Report'}
            onPress={this.submitReport}
          />
        </Form>
      </ScrollView>
    );
  }
}
