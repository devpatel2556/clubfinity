import React, { Component } from 'react';
import {
  Button, ScrollView, VStack,
} from 'native-base';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { DateTime } from 'luxon';
import {
  StyleSheet, View, Text,
} from 'react-native';
import RenderHTML from 'react-native-render-html';
import colors from '../util/colors';
import standardColors from '../constants/Colors';
import UserContext from '../util/UserContext';
import Layout from '../constants/Layout';
import ResponseButton from '../components/ResponseButton';
import EventsApi from '../api/EventsApi';
import StatisticsCard from '../components/StatisticsCard';

const style = StyleSheet.create({
  eventsPage: {
    width: '85%',
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: '5%',
    marginRight: '7.5%',
    marginLeft: '7.5%',
    marginTop: '8%',
    marginBottom: '8%',
  },
  detailListItem: {
    width: '100%',
    height: 45,
    flexDirection: 'row',
    alignItems: 'center',
  },
  editButton: {
    alignSelf: 'center',
    backgroundColor: colors.secondary0,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: '1%',
    marginBottom: '5%',
    marginTop: 20,
    borderRadius: 5,
  },
  statisticsCardContainer: {
    flexDirection: 'row',
    marginTop: 6,
    justifyContent: 'space-around',
    width: '100%',
    display: 'flex',
    flexWrap: 'wrap',
  },
  responseButtonContainer: {
    flexDirection: 'row',
    marginTop: 5,
    width: '100%',
  },
  label: {
    fontSize: 18,
    fontWeight: 'bold',
    width: '100%',
    marginBottom: 12,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  fullWidth: {
    width: '100%',
  },
  horizontalLine: {
    height: 1,
    width: '100%',
    backgroundColor: standardColors.grayFade,
    marginTop: 22,
    marginBottom: 18,
  },
  horizontalLine2: {
    height: 1,
    width: '100%',
    backgroundColor: standardColors.grayFade,
    marginTop: 18,
    marginBottom: 16,
  },
  dayNumber: {
    color: standardColors.black,
    fontSize: 24,
    textAlign: 'center',
  },
  month: {
    color: standardColors.red,
    fontSize: 14,
  },
  shortDateContainer: {
    flexDirection: 'column',
    alignItems: 'center',
    marginRight: 24,
    marginLeft: 10,
  },
  eventHeader: {
    width: '100%',
    height: '12%',
    flexDirection: 'row',
    alignItems: 'center',
  },
  titleContainer: {
    height: 45,
    marginBottom: '5%',
    display: 'flex',
  },
  icon: {
    marginRight: '5%',
  },
});

export default class EventScr extends Component {
  static contextType = UserContext;

  constructor(props) {
    super(props);
    this.state = {
      going: false,
      uninterested: false,
      interested: false,
      numGoing: 0,
      numUninterested: 0,
      numInterested: 0,
      isAdmin: false,
      event: { club: {} },
    };
  }

  componentDidMount() {
    const { route } = this.props;
    const { user } = this.context;
    const { event } = route.params;
    const { goingUsers, uninterestedUsers, interestedUsers } = event;

    const going = goingUsers.includes(user._id);
    const uninterested = uninterestedUsers.includes(user._id);
    const interested = interestedUsers.includes(user._id);

    const isAdmin = event.club.admins
      .map((admin) => admin._id)
      .includes(user._id);

    this.setState({
      going,
      uninterested,
      interested,
      numGoing: goingUsers.length - going,
      numUninterested: uninterestedUsers.length - uninterested,
      numInterested: interestedUsers.length - interested,
      isAdmin,
      event,
    });
  }

  goingHandler = async () => {
    const { route } = this.props;
    const { updateStatus } = route.params;
    const { event, going: previouslyGoing } = this.state;

    if (previouslyGoing) {
      this.setState({
        going: false,
      });

      await EventsApi.removeGoingUser(event._id);
    } else {
      this.setState({
        going: true,
        interested: false,
        uninterested: false,
      });

      await EventsApi.addGoingUser(event._id);
    }
    updateStatus();
  };

  uninterestedHandler = async () => {
    const { route } = this.props;
    const { updateStatus } = route.params;
    const { event, uninterested: previouslyUninterested } = this.state;

    if (previouslyUninterested) {
      this.setState({
        uninterested: false,
      });

      await EventsApi.removeUninterestedUser(event._id);
    } else {
      this.setState({
        uninterested: true,
        interested: false,
        going: false,
      });

      await EventsApi.addUninterestedUser(event._id);
    }
    updateStatus();
  };

  interestedHandler = async () => {
    const { route } = this.props;
    const { updateStatus } = route.params;
    const { event, interested: previouslyInterested } = this.state;

    if (previouslyInterested) {
      this.setState({
        interested: false,
      });

      await EventsApi.removeInterestedUser(event._id);
    } else {
      this.setState({
        interested: true,
        uninterested: false,
        going: false,
      });

      await EventsApi.addInterestedUser(event._id);
    }
    updateStatus();
  };

  render() {
    const {
      going,
      uninterested,
      interested,
      numGoing,
      numInterested,
      numUninterested,
      isAdmin,
      event,
    } = this.state;

    const { navigation } = this.props;
    const datetime = DateTime.fromISO(event.date);
    let month = '';
    if (datetime.monthShort != null) {
      month = datetime.monthShort.toString().toUpperCase();
    }

    return (
      <ScrollView>
        <View style={style.eventsPage}>
          <View style={style.eventHeader}>
            <View style={style.shortDateContainer}>
              <Text style={style.dayNumber}>{datetime.day}</Text>
              <Text style={style.month}>{month}</Text>
            </View>
            <View style={{ height: '100%', justifyContent: 'center' }}>
              <Text style={style.title}>{event.name}</Text>
            </View>
          </View>
          <View style={style.responseButtonContainer}>
            <ResponseButton
              clickHandler={this.goingHandler}
              selected={going}
              icon="ios-checkmark-circle-outline"
              label="Going"
            />
            <View style={{ marginRight: 33, marginLeft: 33 }}>
              <ResponseButton
                clickHandler={this.interestedHandler}
                selected={interested}
                icon="md-star-outline"
                label="Interested"
              />
            </View>
            <ResponseButton
              clickHandler={this.uninterestedHandler}
              selected={uninterested}
              icon="md-close"
              label="Not Going"
            />
          </View>
          {isAdmin && (
          <Button
            style={style.editButton}
            onPress={() => navigation.navigate('Edit Event', { event })}
          >
            <Text style={{
              alignSelf: 'center', color: 'white', fontWeight: '600', padding: 3,
            }}
            >
              EDIT
            </Text>
          </Button>
          )}
          <View style={style.horizontalLine} />
          <View style={style.detailListItem} stackedLabel>
            <MaterialCommunityIcons
              name="account-group"
              size={24}
              style={style.icon}
            />
            <Text>{event.club.name}</Text>
          </View>
          <View style={style.detailListItem} stackedLabel>
            <MaterialCommunityIcons
              name="map-marker"
              size={24}
              style={style.icon}
            />
            <Text>{event.location}</Text>
          </View>
          <View style={style.detailListItem} stackedLabel>
            <MaterialCommunityIcons
              name="clock"
              size={24}
              style={style.icon}
            />
            <Text>{datetime.toLocaleString(DateTime.DATETIME_MED)}</Text>
          </View>
          <View style={style.horizontalLine2} />
          <VStack width="100%">
            <Text style={style.label}>Description</Text>
            {event?.description
            && <RenderHTML source={{ html: event.description }} contentWidth={Layout.window.width} />}
          </VStack>
          <View style={style.horizontalLine} />
          <Text style={style.label}>Responses</Text>
          <View style={style.statisticsCardContainer}>
            <StatisticsCard
              icon="md-checkmark-circle-outline"
              number={numGoing + going}
              label="Going"
            />
            <StatisticsCard
              icon="ios-star-outline"
              number={numInterested + interested}
              label="Interested"
            />
            <StatisticsCard
              icon="ios-close-circle-outline"
              number={numUninterested + uninterested}
              label="Not Going"
            />
          </View>
        </View>
      </ScrollView>
    );
  }
}
