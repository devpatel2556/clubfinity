import React, { useContext } from 'react';
import * as SecureStore from 'expo-secure-store';
import { ScrollView, VStack } from 'native-base';
import SettingsListItem from '../components/SettingsListItem';
import UserContext from '../util/UserContext';

import { resetRoot } from '../navigation/helpers/resetNavigationHelper';

const SettingScr = (props) => {
  const userContext = useContext(UserContext);

  const signOut = async () => {
    await SecureStore.deleteItemAsync('userToken');
    const { setUser } = userContext;

    resetRoot('AuthStack');
    setUser(null);
  };

  const { navigation } = props;
  return (
    <ScrollView>
      <VStack>
        <SettingsListItem
          onPress={() => navigation.navigate('Edit Profile')}
          icon="md-person"
          label="Edit Profile"
        />
        <SettingsListItem
          onPress={() => navigation.navigate('Submit Club')}
          icon="md-create"
          label="Submit a Club"
        />
        <SettingsListItem
          onPress={() => navigation.navigate('Edit Notifications')}
          icon="md-notifications"
          label="Notifications"
        />
        <SettingsListItem
          onPress={() => navigation.navigate('Report Bug')}
          icon="md-bug"
          label="Report a Bug"
        />
        <SettingsListItem
          onPress={() => navigation.navigate('Delete')}
          icon="md-sad"
          label="Delete My Account"
        />
        <SettingsListItem
          onPress={() => signOut()}
          icon="md-log-out"
          label="Logout"
        />
      </VStack>
    </ScrollView>
  );
};

export default SettingScr;
