import React, { Component } from 'react';
import {
  TouchableOpacity,
  ActivityIndicator,
  View,
  FlatList,
} from 'react-native';
import { Ionicons, MaterialIcons } from '@expo/vector-icons';
import {
  Card,
  Box,
  Input,
  HStack,
  List,
  Text,
  Image,
} from 'native-base';
import RenderHTML from 'react-native-render-html';
import CustomRefresh from '../components/CustomRefresh';
import ClubsApi from '../api/ClubsApi';
import Layout from '../constants/Layout';
import colors from '../util/colors';
import defaultClubImage from '../assets/images/DefaultClubImage.png';

const LINE_CHARACTER_REQ = 40;

export default class DiscoverScr extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchText: '',
      clubs: '',
      filteredClubs: '',
      isLoading: true,
    };
  }

  componentDidMount() {
    const { navigation } = this.props;
    navigation.addListener('willFocus', this.onFocus);
    this.onFocus();
  }

  changeHandler = (newState) => {
    this.setState(newState);
  }

  onFocus = async () => {
    try {
      const data = await ClubsApi.getAllClubs();
      this.setState({
        clubs: data,
        filteredClubs: data,
        isLoading: false,
      });
    } catch (error) {
      console.error(error);
    }
  };

  handleClubSelect = (club, clubImage) => {
    const { navigation } = this.props;
    navigation.navigate('Club', {
      club,
      clubImage,
    });
  };

  filterClubs = (text) => {
    const searchText = text.toLowerCase();
    const { clubs } = this.state;
    const newFilterClubs = clubs.filter((club) => club.name.toLowerCase().includes(searchText));

    this.setState({
      searchText: text,
      filteredClubs: newFilterClubs,
    });
  };

  renderSearch = () => {
    const { searchText } = this.state;
    return (
      <HStack
        rounded
        searchBar
        style={{
          borderBottomWidth: 0,
        }}
      >
        <Box
          style={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'flex-start',
            width: '90%',
            marginLeft: '5%',
            height: 35,
            backgroundColor: 'transparent',
            borderBottomWidth: 0.9,
            borderColor: '#000000',
          }}
        >
          <Input
            placeholder="Explore clubs"
            onChangeText={(text) => this.filterClubs(text)}
            value={searchText}
            autoCorrect={false}
            variant="unstyled"
            style={{
              fontSize: 14,
            }}
            InputLeftElement={(
              <Ionicons
                name="ios-search"
                size={20}
                style={{
                  marginLeft: '2%',
                }}
                color={colors.grayScale8}
              />
            )}
            InputRightElement={
            (searchText !== '')
              ? (
                <TouchableOpacity onPress={() => this.filterClubs('')}>
                  <MaterialIcons
                    name="clear"
                    size={20}
                    style={{
                      marginTop: '1%',
                      marginRight: '3%',
                    }}
                    color={colors.grayScale8}
                  />
                </TouchableOpacity>
              )
              : null
          }
          />
        </Box>
      </HStack>
    );
  };

  render() {
    const { isLoading, filteredClubs } = this.state;
    if (isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <ActivityIndicator />
        </View>
      );
    }

    return (
      <View style={{ backgroundColor: '#f5f6fa', flex: 1 }}>
        <View>
          {/* Grid */}
          <List
            style={{
              width: '100%',
            }}
          >
            <FlatList
              refreshControl={(
                <CustomRefresh
                  changeHandler={this.changeHandler}
                  reqs={{ screen: 'DiscoverScr' }}
                />
              )}
              data={filteredClubs}
              contentContainerStyle={{ flexGrow: 1 }}
              style={{
                width: '100%',
                height: '100%',
              }}
              renderItem={({ item }) => (
                <TouchableOpacity
                  style={{
                    width: '95%',
                    alignSelf: 'center',
                  }}
                  onPress={() => this.handleClubSelect(
                    item,
                    item.thumbnailUrl
                      ? { uri: item.thumbnailUrl }
                      : defaultClubImage,
                  )}
                >
                  <View
                    style={{
                      height: '100%',
                      width: '100%',
                      top: '5%',
                      alignSelf: 'center',
                      display: 'flex',
                      flexDirection: 'row',
                      flex: 1,
                    }}
                  >
                    <Card
                      style={{
                        display: 'flex',
                        flexDirection: 'row',
                        width: '100%',
                        height: '100%',
                        alignItems: 'center',
                        backgroundColor: colors.grayScale0,
                        borderWidth: 1,
                        borderColor: colors.grayScale3,
                        marginBottom: '-9%',
                      }}
                    >
                      <Box>
                        <Image
                          size="12"
                          source={
                                    item.thumbnailUrl
                                      ? { uri: item.thumbnailUrl }
                                      : defaultClubImage
                                  }
                          alt="react-native"
                          borderRadius={50}
                        />
                      </Box>
                      <View
                        style={{ display: 'flex', flexDirection: 'column', marginLeft: '5%' }}
                      >
                        <Box
                          header
                          style={{
                            display: 'flex',
                            flexDirection: 'column',
                            alignContent: 'flex-start',
                            alignItems: 'flex-start',
                            paddingTop: '1%',
                            paddingBottom: '1%',
                          }}
                        >
                          <Text style={{ paddingTop: '2%' }}>{item.name}</Text>
                          <Text
                            style={{
                              fontSize: 12,
                              color: colors.grayScale8,
                            }}
                          >
                            {item.category}
                          </Text>
                        </Box>
                        <Box cardBody>
                          <Text
                            style={{
                              fontSize: 14,
                              color: colors.grayScale8,
                              paddingBottom: '5%',
                            }}
                          >
                            {item.description.length > LINE_CHARACTER_REQ
                              // eslint-disable-next-line max-len
                              ? (
                                <RenderHTML
                                  source={{ html: `${item.description.substring(0, LINE_CHARACTER_REQ).trim()}...` }}
                                  contentWidth={Layout.window.width}
                                />
                              )
                              : (
                                <RenderHTML
                                  source={{ html: item.description.trim() }}
                                  contentWidth={Layout.window.width}
                                />
                              )}
                          </Text>
                        </Box>
                      </View>
                      <View style={{ marginLeft: 'auto' }}>
                        <Ionicons
                          name="chevron-forward-outline"
                          size={30}
                          style={{ paddingRight: '1%' }}
                        />
                      </View>
                    </Card>
                  </View>
                </TouchableOpacity>
              )}
              numColumns={1}
              keyExtractor={(item) => item._id}
              ListHeaderComponent={this.renderSearch}
            />
          </List>
        </View>
      </View>
    );
  }
}
