import React, { Component } from 'react';
import { Platform } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { DateTime } from 'luxon';
import EventsApi from '../api/EventsApi';
import Form from '../components/Form';
import { updateStateAndClearErrors } from '../util/formUtil';
import Validator from '../components/form/validation/Validator';
import UserContext from '../util/UserContext';

export default class EventCreation extends Component {
  static contextType = UserContext;

  validator = new Validator({
    name: [Validator.required(), Validator.minLength(3)],
    location: [Validator.required(), Validator.minLength(3)],
    facebookLink: [Validator.validFacebookUrl()],
    description: [Validator.required()],
  });

  constructor(props) {
    super(props);

    this.state = {
      name: '',
      date: DateTime.local(),
      location: '',
      facebookLink: '',
      description: '',
      errors: {},
    };
  }

  createEvent = async () => {
    const validationResult = this.validator.validate(this.state);
    const { setMessage } = this.context;

    if (!validationResult.valid) {
      this.setState({ errors: validationResult.errors });

      return;
    }

    const {
      name,
      description,
      facebookLink,
      location,
      date,
    } = this.state;
    const { route, navigation } = this.props;

    const { club } = route.params;

    await EventsApi.create({
      club: club._id,
      name,
      description,
      facebookLink,
      date: date.toISO(),
      location,
    });
    // Handle event creation error.
    setMessage('Event created.');
    navigation.pop();
  };

  setShowDatePicker = () => {
    this.setState({ showDatePicker: true });
  };

  setShowTimePicker = () => {
    this.setState({ showTimePicker: true });
  };

  render() {
    const {
      name, date, location, facebookLink, description, errors,
    } = this.state;

    return (
      <KeyboardAwareScrollView
        resetScrollToCoords={{ x: 0, y: 0 }}
      >
        <Form>
          <Form.Text
            placeholder="Name"
            value={name}
            onChange={(value) => updateStateAndClearErrors(this, 'name', value)}
            error={errors.name}
          />
          <Form.Date
            value={date}
            onChange={(value) => updateStateAndClearErrors(this, 'date', value)}
          />
          <Form.Text
            placeholder="Location"
            value={location}
            onChange={(value) => updateStateAndClearErrors(this, 'location', value)}
            error={errors.location}
          />
          <Form.Text
            placeholder="Facebook Link"
            value={facebookLink}
            onChange={(value) => updateStateAndClearErrors(this, 'facebookLink', value)}
            error={errors.facebookLink}
            autoCapitalize="none"
            autoCorrect={false}
            keyboardType={Platform.OS === 'ios' ? 'url' : 'default'}
          />
          <Form.TextArea
            placeholder="Description"
            value={description}
            onChange={(value) => updateStateAndClearErrors(this, 'description', value)}
            error={errors.description}
          />
          <Form.Button text="Create Event" onPress={this.createEvent} />
        </Form>
      </KeyboardAwareScrollView>
    );
  }
}
