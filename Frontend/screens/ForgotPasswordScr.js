import React from 'react';
import {
  View,
  Dimensions,
  SafeAreaView,
  StatusBar,
  Image,
  StyleSheet,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import ClubfinityLogo from '../assets/images/ClubfinityLogo.png';
import UserApi from '../api/UserApi';
import UserContext from '../util/UserContext';
import colors from '../util/colors';
import Form from '../components/Form';

const MAX_FIELD_WIDTH = (Dimensions.get('screen').width * 4) / 5;
const STATUS_BAR_HEIGHT = StatusBar.currentHeight;

const styles = StyleSheet.create({
  mainContainer: {
    margin: 20,
    marginTop: STATUS_BAR_HEIGHT,
    flex: 1,
    display: 'flex',
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 34,
    letterSpacing: 1,
    marginVertical: 30,
  },
  field: {
    borderBottomColor: colors.grayScale1,
    borderBottomWidth: 2,
    minWidth: MAX_FIELD_WIDTH,
    borderColor: colors.grayScale1,
    margin: 8,
    paddingVertical: 9,
  },
  loginButton: {
    padding: 10,
    minWidth: MAX_FIELD_WIDTH,
    backgroundColor: colors.primary0,
    borderColor: colors.primary1,
    borderRadius: 8,
    marginHorizontal: 12,
    marginVertical: 12,
    minHeight: 42,
    elevation: 3,
  },
  loginButtonText: {
    fontSize: 15,
    alignSelf: 'center',
    color: colors.grayScale0,

  },

  signupLink: {
    borderBottomColor: colors.primary0,
    borderBottomWidth: 1,
    top: 5,
  },
});

export default class ForgotYourPasswordScr extends React.Component {
    static navigationOptions = {
      header: null,
    };

    static contextType = UserContext;

    constructor(props) {
      super(props);
      this.state = {
        email: '',
      };
    }

    changeEmail = (input) => {
      this.setState({
        email: input,
      });
    };

    redirectToVerificationCodeInput = (email, id) => {
      const { navigation } = this.props;

      navigation.navigate('ForgotPasswordVerification', {
        email,
        id,
      });
    }

    resetPasswordRequest = async (event) => {
      event.preventDefault();
      const { email } = this.state;
      const emailResponse = (await UserApi.forgotPasswordEmail(email)).data;
      if (emailResponse) {
        this.redirectToVerificationCodeInput(email, emailResponse._id);
      } else {
        alert('Can\'t find email. Try signing up again with the same email.');
      }
    }

    render() {
      const {
        email,
      } = this.state;

      return (
        <KeyboardAwareScrollView
          resetScrollToCoords={{ x: 0, y: 0 }}
          contentContainerStyle={{ flex: 1 }}
          scrollEnabled={false}
        >
          <SafeAreaView style={styles.mainContainer}>
            <View
              style={{
                flex: 1,
                justifyContent: 'space-between',
                alignItems: 'center',
              }}
            >
              <Image
                style={{
                  width: 200,
                  height: 200,
                  margin: 30,
                  marginBottom: 80,
                }}
                source={ClubfinityLogo}
              />
              <View style={{ flex: 1, minWidth: MAX_FIELD_WIDTH }}>
                <Form style={{ padding: 0 }}>
                  <Form.Text
                    placeholder="Email"
                    value={email}
                    onChange={this.changeEmail}
                    autoCapitalize="none"
                    returnKeyType="next"
                    keyboardType="email-address"
                  />
                  <Form.Button
                    text="Reset password"
                    onPress={this.resetPasswordRequest}
                  />
                </Form>
                <View
                  style={{
                    flex: 1,
                  }}
                />
              </View>
            </View>
          </SafeAreaView>

        </KeyboardAwareScrollView>
      );
    }
}
