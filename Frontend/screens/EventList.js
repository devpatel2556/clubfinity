import React, { Component } from 'react';
import {
  View,
  SectionList,
  Text,
} from 'react-native';
import { DateTime } from 'luxon';
import CustomRefresh from '../components/CustomRefresh';
import EventsApi from '../api/EventsApi';
import Row from '../components/Row';

class EventList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pastEvents: [],
      upcomingEvents: [],
      loading: false,
    };
  }

  async componentDidMount() {
    const { route } = this.props;
    const { clubId } = route.params;
    this.setState({ loading: true });
    const events = await EventsApi.getForClub(clubId);
    const now = DateTime.local();
    const pastEvents = events
      .filter((event) => event.date < now)
      .sort((a, b) => Date.parse(b.date) - Date.parse(a.date));
    const upcomingEvents = events.filter((event) => event.date > now);

    this.setState({ pastEvents, upcomingEvents, loading: false });
  }

  changeHandler = (newState) => {
    this.setState(newState);
  }

  renderSectionHeader = (section) => (
    <Text
      style={{
        fontWeight: 'bold',
        fontSize: 18,
        marginBottom: 15,
        backgroundColor: 'white',
      }}
    >
      {section.title}
    </Text>
  )

  renderEmpty = () => (
    <Text style={{ color: 'rgba(0, 0, 0, 0.50)', textAlign: 'center' }}>No Upcoming or Past Events</Text>
  )

  renderLoading = () => (
    <View style={{ flex: 1, paddingVertical: 20, paddingHorizontal: 30 }}>
      <Text style={{ color: 'rgba(0, 0, 0, 0.50)', textAlign: 'center' }}>Loading Events...</Text>
    </View>
  )

  render() {
    const { navigation, route } = this.props;
    const { upcomingEvents, pastEvents, loading } = this.state;
    const { clubId } = route.params;

    if (loading) return this.renderLoading();

    const listData = [];
    if (upcomingEvents.length > 0) {
      listData.push({ title: 'Upcoming Events', data: upcomingEvents });
    }

    if (pastEvents.length > 0) {
      listData.push({ title: 'Past Events', data: pastEvents });
    }

    return (
      <View style={{ flex: 1, paddingVertical: 20, paddingHorizontal: 30 }}>
        <SectionList
          refreshControl={(
            <CustomRefresh
              changeHandler={this.changeHandler}
              reqs={{ screen: 'EventList', clubId }}
            />
          )}
          sections={listData}
          keyExtractor={(event) => event._id}
          renderItem={({ item }) => (
            <Row
              date={item.date.toFormat('MMM dd yyyy')}
              text={item.name}
              handler={() => {
                navigation.navigate('Event', { event: item });
              }}
            />
          )}
          renderSectionHeader={({ section }) => this.renderSectionHeader(section)}
          ListEmptyComponent={this.renderEmpty}
        />
      </View>
    );
  }
}

export default EventList;
