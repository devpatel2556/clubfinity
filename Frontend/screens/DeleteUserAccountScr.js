import React, { useContext } from 'react';
import { View, StyleSheet } from 'react-native';
import * as SecureStore from 'expo-secure-store';
import { Button, Text } from 'native-base';
import colors from '../util/colors';
import { resetRoot } from '../navigation/helpers/resetNavigationHelper';

import UserContext from '../util/UserContext';
import UserApi from '../api/UserApi';

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  confirmButton: {
    marginTop: '4%',
    alignSelf: 'center',
    backgroundColor: colors.primary0,
    width: '85%',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const DeleteUserAccountScr = (props) => {
  const userContext = useContext(UserContext);
  const signOut = async () => {
    await SecureStore.deleteItemAsync('userToken');
    resetRoot('AuthStack');
    const { setUser } = userContext;
    setUser(null);
  };

  const { navigation } = props;

  const deleteAccountHandler = async () => {
    const { user } = userContext;
    const userId = user._id;
    const deleteUserResponse = await UserApi.deleteUser(userId);
    if (deleteUserResponse) signOut();
    return deleteUserResponse;
  };

  return (
    <View style={styles.mainContainer}>
      <Text>
        Are you sure you want to delete your account? You will be deleted from
        each club you are a member of.
      </Text>
      <Button
        style={styles.confirmButton}
        onPress={() => deleteAccountHandler()}
      >
        <Text>Yes, I would like to delete my account</Text>
      </Button>
      <Button
        style={styles.confirmButton}
        onPress={() => navigation.navigate('Settings')}
      >
        <Text>No, I would like to keep my account</Text>
      </Button>
    </View>
  );
};

export default DeleteUserAccountScr;
