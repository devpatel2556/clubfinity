import React from 'react';
import {
  Text, View, StyleSheet, TextInput,
} from 'react-native';
import { FontAwesome5 } from '@expo/vector-icons';
import Form from '../components/Form';
import colors from '../util/colors';
import SubmitButton from '../components/form/Button';
import UserApi from '../api/UserApi';
import AuthApi from '../api/AuthApi';
import UserContext from '../util/UserContext';
import SlideDownNotification from '../components/SlideDownNotification';

const styles = StyleSheet.create({
  container: {
    textAlign: 'center',
    marginTop: '25%',
    margin: '7%',
    flex: 1,
  },
  resentEmailNotificationContainer: {
    marginLeft: 'auto',
    marginRight: 'auto',
    width: '92%',

    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1.5,
    borderColor: colors.info,
    borderRadius: 10,
    backgroundColor: 'white',

    padding: 12,

    shadowColor: 'black',
    shadowOpacity: 0.15,
  },
  resentEmailNotificationText: {
    color: colors.info,
    marginLeft: 7,
  },
  header: {
    fontSize: 24,
    lineHeight: 32,
    color: colors.grayScale9,
    textAlign: 'center',

    marginTop: '25%',
    marginBottom: 20,
  },
  body: {
    fontSize: 14,
    color: colors.grayScale9,
    textAlign: 'center',
    marginBottom: 20,
  },
  bold: {
    fontWeight: 'bold',
  },
  codeInput: {
    borderRadius: 10,
    backgroundColor: colors.grayScale1,
    padding: 0,
  },
  codeInputError: {
    borderWidth: 1,
    borderColor: colors.error,
  },
  sendEmailAgainContainer: {
    textAlign: 'center',
    marginTop: 20,
    color: colors.grayScale9,
  },
  sendEmailAgainLink: {
    fontSize: 14,
    top: 2,
    textDecorationLine: 'underline',
  },
  bottomContainer: {
    display: 'flex',
    justifyContent: 'flex-end',
    flex: 1,
    marginTop: '50%',
  },
  signUpAgainContainer: {
    color: colors.grayScale9,
    fontSize: 14,
    textAlign: 'center',
    flex: 1,
  },
  signUpAgainLink: {
    textDecorationLine: 'underline',
  },
  verifyButton: {
    marginTop: 10,
  },
});

export default class ForgotPasswordVerification extends React.Component {
  static navigationOptions = {
    header: null,
  };

  static contextType = UserContext;

  constructor(props) {
    super(props);

    this.state = {
      code: '',
      password: '',
      verifyPassword: '',
      error: null,
      isProcessing: false,
      displayResentNotification: false,
    };
  }

  changeCode = (code) => {
    this.setState({ code, error: null });
  };

  changePassword = (password) => {
    this.setState({ password, error: null });
  };

  changeVerifyPassword = (verifyPassword) => {
    this.setState({ verifyPassword, error: null });
  };

  tempLogin = async (event) => {
    event.preventDefault();
    const { route } = this.props;
    const { setUser } = this.context;
    const { email, id } = route.params;
    const { code, password, verifyPassword } = this.state;

    if (password !== verifyPassword) {
      alert('Passwords do not match');
      return;
    }
    if (password.length < 6) {
      alert('Password must be at least 6 characters');
      return;
    }
    if (code.length === 0) {
      alert('Please enter a code');
      return;
    }

    const resetResponse = await UserApi.resetPassword(code, password, id);
    if (!resetResponse.ok) {
      alert('Invalid code');
      return;
    }

    this.setState({ error: null });
    const authResponse = await AuthApi.authenticate(email, password);
    if (authResponse.token) {
      if (authResponse.user) {
        setUser(authResponse.user);
      } else {
        alert('auth response user failed');
      }
    } else {
      alert('auth response token failed');
    }
  };

  hideResentNotification = () => {
    this.setState({ displayResentNotification: false });
  };

  resendEmail = async () => {
    const { route } = this.props;
    const { userId } = route.params;
    const resp = await UserApi.resendEmailVerificationCode(userId);
    const { setMessage } = this.context;

    // Handle error when resending email.

    if (resp.ok) {
      setMessage('Email verification sent.');
      this.setState({
        displayResentNotification: true,
      });
    }
  };

  goBackToSignUp = () => {
    const { navigation } = this.props;
    navigation.pop();
    navigation.pop();
    navigation.navigate('SignUp');
  };

  clearError = () => {
    this.setState({ error: null });
  };

  renderResendNotification = () => {
    const { displayResentNotification } = this.state;

    if (!displayResentNotification) {
      return null;
    }

    const component = (
      <View style={styles.resentEmailNotificationContainer}>
        <FontAwesome5 name="check-circle" size={18} color={colors.info} />
        <Text style={styles.resentEmailNotificationText}>EMAIL RESENT!</Text>
      </View>
    );

    return (
      <SlideDownNotification
        component={component}
        onAnimationFinish={this.hideResentNotification}
      />
    );
  };

  renderCodeInput = () => {
    const { error, code } = this.state;

    if (error) {
      return (
        <Form.Text
          style={{ ...styles.codeInput, ...styles.codeInputError }}
          placeholder={error}
          placeholderTextColor={colors.error}
          keyboardType="number-pad"
          // returnKeyType="done"
          value={code}
          onChangeText={this.changeCode}
          onFocus={this.clearError}
        />
      );
    }

    return (
      <Form.Text
        style={styles.codeInput}
        placeholder="Code"
        placeholderTextColor={colors.grayScale7}
        keyboardType="number-pad"
        // returnKeyType="done"
        value={code}
        onChangeText={this.changeCode}
      />
    );
  };

  renderPasswordInput = () => {
    const { error, password, verifyPassword } = this.state;

    if (error) {
      return (
        <TextInput
          style={{ ...styles.codeInput, ...styles.codeInputError }}
          placeholder={error}
          placeholderTextColor={colors.error}
          // keyboardType="number-pad"
          returnKeyType="done"
          value="s"
          onChangeText={this.changeCode}
          onFocus={this.clearError}
        />
      );
    }

    return (
      <View>
        <Form.Text
          style={styles.codeInput}
          placeholder="Password"
          placeholderTextColor={colors.grayScale7}
          keyboardType="default"
          // returnKeyType="done"
          value={password}
          onChangeText={this.changePassword}
        />
        <Form.Text
          style={styles.codeInput}
          placeholder="Verify Password"
          placeholderTextColor={colors.grayScale7}
          keyboardType="default"
          // returnKeyType="done"
          value={verifyPassword}
          onChangeText={this.changeVerifyPassword}
        />
      </View>
    );
  };

  render() {
    const { isProcessing } = this.state;
    const { route } = this.props;
    const { email } = route.params;

    return (
      <View style={styles.container}>
        {this.renderResendNotification()}
        <View>
          <Text style={styles.header}>
            Set your new password
          </Text>
          <Text style={styles.body}>
            An email with a verification code has been sent to&nbsp;
            <Text style={styles.bold}>{email}</Text>
            &nbsp;Please enter the code below within 15 minutes along with your new password.
          </Text>

          {this.renderCodeInput()}

          {this.renderPasswordInput()}

          <SubmitButton
            style={styles.verifyButton}
            text={isProcessing ? 'Verifying...' : 'Verify'}
            onPress={this.tempLogin}
          />
          <Text style={styles.sendEmailAgainContainer}>
            Didn&apos;t receive the email?&nbsp;
            <Text
              style={styles.sendEmailAgainLink}
              onPress={this.resendEmail}
            >
              Send it again!
            </Text>
          </Text>
        </View>
        <View style={styles.bottomContainer}>
          <Text style={styles.signUpAgainContainer}>
            Incorrect email?&nbsp;
            <Text
              style={styles.signUpAgainLink}
              onPress={this.goBackToSignUp}
            >
              Sign up again!
            </Text>
          </Text>
        </View>
      </View>
    );
  }
}
