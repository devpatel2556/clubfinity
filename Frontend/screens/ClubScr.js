import React, { useContext, useEffect, useState } from 'react';
import {
  View,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  StyleSheet,
  Linking,
} from 'react-native';
import {
  Button,
  Box,
  Text,
  Heading,
  Image,
  VStack,
  Flex,
} from 'native-base';
import { useNavigation } from '@react-navigation/native';
import { Ionicons } from '@expo/vector-icons';
import RenderHTML from 'react-native-render-html';
import { DateTime } from 'luxon';
import colors from '../util/colors';
import Layout from '../constants/Layout';
import UserContext from '../util/UserContext';
import UserApi from '../api/UserApi';
import ClubsApi from '../api/ClubsApi';
import AdminRow from '../components/AdminRow';
import CustomRefresh from '../components/CustomRefresh';
import defaultClubImage from '../assets/images/DefaultClubImage.png';

const LINE_CHARACTER_REQ = 30;
const styles = StyleSheet.create({
  clubView: {
    display: 'flex',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  adminButton: {
    marginTop: '4%',
    alignSelf: 'center',
    backgroundColor: colors.secondary0,
    width: '85%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  clubScrCard: {
    width: '85%',
    marginTop: '10%',
    borderTopColor: colors.primary0,
    borderTopWidth: 10,
    borderRadius: 3,
    padding: '4%',
    borderWidth: 1,
    borderColor: 'lightgray',
  },
  noAnnouncementsEventsMessage: {
    alignSelf: 'center',
    opacity: 0.7,
    marginBottom: '5%',
    fontSize: 14,
  },
  listTextWrapper: {
    width: '100%',
    marginLeft: '0%',
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection: 'row',
    padding: '2%',
    paddingTop: '2%',
    alignItems: 'center',
  },
  fullWidth: {
    width: '100%',
  },
});

const ClubScr = ({ route }) => {
  const { club: clubParams } = route.params;

  const { user, setUser } = useContext(UserContext);
  const navigation = useNavigation();

  const [isFollowing, setIsFollowing] = useState(false);
  const [isAdmin, setIsAdmin] = useState(false);
  const [club, setClub] = useState(clubParams);
  const [events, setEvents] = useState([]);
  const [announcements, setAnnouncements] = useState([]);

  const announcementsEmpty = announcements.length === 0;
  const eventsEmpty = events.length === 0;

  // Function to run on each render -> fetch latest events/annoucements
  const onFocus = async () => {
    const { events: initialEvents, announcements: initialAnnouncements } = await ClubsApi.getPosts(clubParams._id);
    const today = Date.parse(new Date());
    const upcomingEvents = initialEvents.filter((event) => Date.parse(event.date) >= today);
    setEvents(upcomingEvents);
    setAnnouncements(initialAnnouncements.reverse());
  };

  // Check if current user is an admin in the club param
  const checkIfAdmin = (clubObj) => {
    if (clubObj.admins.map((admin) => admin._id).includes(user._id)) {
      setIsAdmin(true);
    } else {
      setIsAdmin(false);
    }
  };

  useEffect(() => {
    // Set initial user state regarding club (following? / is admin?)
    user.clubs.forEach((c) => {
      if (c._id === clubParams._id) {
        setIsFollowing(true);
        checkIfAdmin(c);
      }
    });

    // Set up onFocus function to fetch new events/announcements on each new render
    navigation.addListener('willFocus', onFocus);
    navigation.addListener('focus', onFocus);
    onFocus();
  }, []);

  // Callback function passed to sub-screens to update club screen state
  const updateClubState = (updatedClub) => {
    checkIfAdmin(updatedClub);
    setClub(updatedClub);

    const updatedUserClubs = user.clubs.map((userClub) => {
      if (userClub._id === updatedClub._id) {
        return updatedClub;
      }
      return userClub;
    });
    setUser({ ...user, clubs: updatedUserClubs });
  };

  // Handle follow/unfollow API call
  const handleFollowUpdate = async (apiCall, clubId) => {
    const authResponse = await apiCall(clubId);
    setUser(authResponse.data.data);
    return authResponse;
  };

  // Handle follow/unfollow user button action
  const followBtnHandler = async () => {
    if (!isFollowing) {
      const authResponse = handleFollowUpdate(UserApi.followClub, club?._id);
      if (!authResponse.error) {
        setIsFollowing(true);
      } else {
        console.log('todo: error handling');
      }
    } else {
      handleFollowUpdate(UserApi.unfollowClub, club?._id);
      setIsFollowing(false);
    }
  };

  // Handles any state updates coming from CustomRefresh compoenent (when pull down to refresh)
  const changeHandler = (newState) => {
    const { events: updatedEvents, announcements: updatedAnnouncements, isAdmin: isAdminUpdated } = newState;
    setEvents(updatedEvents);
    setAnnouncements(updatedAnnouncements.reverse());
    setIsAdmin(isAdminUpdated);
  };

  const renderSocialMediaLink = (link, icon, text) => {
    const { name, size, marginRight } = icon;

    return (
      <TouchableOpacity onPress={() => Linking.openURL(link)}>
        <Flex p="2" justify="center" align="center" direction="row">
          <Ionicons name={name} size={size} style={{ marginRight }} />
          <Text paddingLeft="5%" fontSize="md">
            {text}
          </Text>
        </Flex>
      </TouchableOpacity>
    );
  };

  const renderConnectCard = () => {
    if (!club?.slackLink && !club?.facebookLink && !club?.instagramLink) {
      return null;
    }

    return (
      <Box style={styles.clubScrCard}>
        <Box py="2">
          <Text fontSize="lg" bold>
            Connect
          </Text>
        </Box>
        <VStack alignItems="flex-start">
          {!!club?.slackLink
            && renderSocialMediaLink(
              club?.slackLink,
              { name: 'logo-slack', size: 27, marginRight: '4%' },
              'Slack',
            )}
          {!!club?.facebookLink
            && renderSocialMediaLink(
              club?.facebookLink,
              { name: 'logo-facebook', size: 30, marginRight: '5%' },
              'Facebook',
            )}
          {!!club?.instagramLink
            && renderSocialMediaLink(
              club?.instagramLink,
              { name: 'logo-instagram', size: 30, marginRight: '5%' },
              'Instagram',
            )}
        </VStack>
      </Box>
    );
  };

  // TODO: Fix navigation back to the ClubScr from this stack

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <ScrollView
        showsVerticalScrollIndicator
        refreshControl={(
          <CustomRefresh
            changeHandler={changeHandler}
            reqs={{ screen: 'ClubScr', club, user }}
          />
        )}
      >
        <View style={styles.clubView}>
          <View style={{ paddingTop: '10%' }}>
            <Image
              size="40"
              borderRadius={100}
              source={club?.thumbnailUrl ? { uri: club.thumbnailUrl } : defaultClubImage}
              alt="Club Thumbnail"
            />
          </View>
          <Heading
            size="xl"
            textAlign="center"
            px="4"
            style={{ paddingBottom: '2%', paddingTop: '5%' }}
          >
            {club?.name}
          </Heading>
          <Text style={{ paddingBottom: '5%' }}>{club?.category}</Text>

          {isAdmin ? (
            <>
              <Button
                style={styles.adminButton}
                onPress={() => navigation.navigate('Edit Club', { club, updateClubState })}
              >
                <Text>Manage</Text>
              </Button>

              <Button
                style={styles.adminButton}
                onPress={() => navigation.navigate('Create Announcement', { club })}
              >
                <Text>Make an announcement</Text>
              </Button>

              <Button
                style={styles.adminButton}
                onPress={() => navigation.navigate('Create Event', { club })}
              >
                <Text>Create a new event</Text>
              </Button>
            </>
          ) : (
            <Button
              style={{
                alignSelf: 'center',
                backgroundColor: isFollowing
                  ? colors.success
                  : colors.secondary0,
                width: '85%',
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={followBtnHandler}
            >
              {isFollowing ? <Text>Following</Text> : <Text>Follow</Text>}
            </Button>
          )}

          {renderConnectCard()}

          {/**
           * Purpose
           */}
          <Box border="1" py="4" style={styles.clubScrCard}>
            <VStack space="4">
              <Box header>
                <Text fontSize="lg" bold>
                  Purpose
                </Text>
              </Box>
              <Box px="0">
                <Box>
                  {club?.description
                    && (
                    <RenderHTML
                      source={{ html: club.description.substring(0, LINE_CHARACTER_REQ) }}
                      contentWidth={Layout.window.width}
                    />
                    )}
                </Box>
              </Box>
            </VStack>
          </Box>

          {/**
           * Events
           */}
          <Box border="1" style={styles.clubScrCard}>
            <Box py="4" style={{ justifyContent: 'space-between' }}>
              <Text fontSize="lg" bold alignSelf="flex-start">
                Upcoming Events
              </Text>
              {!eventsEmpty && (
                <TouchableOpacity
                  onPress={() => navigation.navigate('Events', {
                    clubId: club?._id,
                    isAdmin,
                  })}
                >
                  <Text style={{ alignSelf: 'flex-start', color: colors.link }}>
                    View all events
                  </Text>
                </TouchableOpacity>
              )}
            </Box>
            <Box style={{ paddingHorizontal: '0%' }}>
              <VStack>
                <Box style={{ paddingHorizontal: '0%', width: '100%' }}>
                  {!eventsEmpty ? (
                    <Box>
                      {[...events].splice(0, 10).map((item) => (
                        <TouchableOpacity
                          onPress={() => navigation.navigate('Event', { event: item, isAdmin })}
                          id={item._id}
                          key={item._id}
                        >
                          <Box
                            borderTopWidth="1"
                            borderColor="gray.200"
                            style={styles.listTextWrapper}
                            width="100%"
                            flex={1}
                          >
                            <Text bold numberOfLines={1} flex="3">
                              {item.name}
                            </Text>
                            <Text fontSize="xs" alignSelf="center">
                              {DateTime.fromISO(item.date).toFormat(
                                'MMM dd yyyy',
                              )}
                            </Text>
                            <Ionicons
                              name="chevron-forward-outline"
                              size={30}
                            />
                          </Box>
                        </TouchableOpacity>
                      ))}
                    </Box>
                  ) : (
                    <Text style={styles.noAnnouncementsEventsMessage}>
                      There are no events for this club.
                    </Text>
                  )}
                </Box>
              </VStack>
            </Box>
          </Box>

          {/**
           * Announcements
           */}
          <Box style={styles.clubScrCard}>
            <VStack>
              <Box py="4" style={{ justifyContent: 'space-between' }}>
                <Text fontSize="lg" bold alignSelf="flex-start">
                  Announcements
                </Text>

                {!announcementsEmpty && (
                  <TouchableOpacity
                    onPress={() => navigation.navigate('Announcements', {
                      clubId: club?._id,
                      isAdmin,
                    })}
                  >
                    <Text
                      style={{ alignSelf: 'flex-start', color: colors.link }}
                    >
                      View all
                    </Text>
                  </TouchableOpacity>
                )}
              </Box>
              <Box style={{ paddingHorizontal: '0%' }}>
                <Box style={{ paddingHorizontal: '0%', width: '100%' }}>
                  {!announcementsEmpty ? (
                    <Box>
                      {[...announcements].splice(0, 5).map((item) => (
                        <TouchableOpacity
                          onPress={() => navigation.navigate('Announcement', { isAdmin, announcement: item })}
                          id={item._id}
                          key={item._id}
                        >
                          <Box
                            borderTopWidth="1"
                            borderColor="gray.200"
                            style={styles.listTextWrapper}
                            width="100%"
                            flex={1}
                          >
                            <VStack width="90%">
                              <Text bold numberOfLines={1} flex="1">
                                {item.title}
                              </Text>
                              <Box
                                marginLeft="3%"
                                maxHeight="35px"
                                overflow="hidden"
                              >
                                <RenderHTML
                                  source={{ html: item.description }}
                                  contentWidth={Layout.window.width}
                                />
                              </Box>
                            </VStack>
                            <Ionicons
                              name="chevron-forward-outline"
                              size={30}
                            />
                          </Box>
                        </TouchableOpacity>
                      ))}
                    </Box>
                  ) : (
                    <Text style={styles.noAnnouncementsEventsMessage}>
                      There are no announcements for this club.
                    </Text>
                  )}
                </Box>
              </Box>
            </VStack>
          </Box>

          {/**
           * Admins
           */}
          <Box py="4" style={styles.clubScrCard}>
            <VStack>
              <Box style={{ justifyContent: 'space-between' }}>
                <Text fontSize="lg" bold alignSelf="flex-start">
                  Admins
                </Text>
                <TouchableOpacity
                  onPress={() => navigation.navigate('Admins', { club, updateClubState })}
                >
                  <Text style={{ alignSelf: 'flex-start', color: colors.link }}>
                    View all
                  </Text>
                </TouchableOpacity>
              </Box>
              <Box style={{ paddingHorizontal: '0%' }}>
                <Box
                  borderTopWidth="1"
                  borderColor="gray.300"
                  paddingHorizontal="0%"
                  width="100%"
                >
                  {club?.admins && club.admins.map((item) => (
                    <AdminRow
                      admin={{
                        name: `${item.name.first} ${item.name.last}`,
                        year: item.year,
                        major: item.major,
                        profileImage: item.profileImage,
                      }}
                      handler={() => {}}
                      id={item._id}
                      key={item._id}
                    />
                  ))}
                </Box>
              </Box>
            </VStack>
          </Box>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default ClubScr;
