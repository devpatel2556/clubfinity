import React, { Component } from 'react';
import { Container, Center } from 'native-base';
import UserContext from '../util/UserContext';
import UserApi from '../api/UserApi';
import Form from '../components/Form';
import Validator from '../components/form/validation/Validator';
import { updateStateAndClearErrors } from '../util/formUtil';

const announcementNotificationOptions = [
  { label: 'When they are posted', value: 'enabled' },
  { label: 'Never', value: 'disabled' },
];
const eventNotificationOptions = [
  { label: 'When they are posted', value: 'enabled' },
  { label: 'Never', value: 'disabled' },
];

const eventReminderNotificationOptions = [
  { label: 'Never', value: 'never' },
  { label: '24 Hours Before', value: '24' },
  { label: '12 Hours Before', value: '12' },
  { label: '6 Hours Before', value: '6' },
  { label: '3 Hours Before', value: '3' },
  { label: '1 Hours Before', value: '1' },
];

export default class EditNotificationScr extends Component {
  static contextType = UserContext;

  validator = new Validator({
    announcementNotifications: [Validator.required()],
    eventNotifications: [Validator.required()],
    eventReminderNotifications: [Validator.required()],
  });

  constructor(props) {
    super(props);
    this.state = {
      announcementNotifications: '',
      eventNotifications: '',
      eventReminderNotifications: '',
      processingRequest: { status: false, message: '' },
      errors: {},
    };
  }

  componentDidMount() {
    const { user } = this.context;
    this.setState(
      {
        announcementNotifications: user.settings.announcementNotifications,
        eventNotifications: user.settings.eventNotifications,
        eventReminderNotifications: user.settings.eventReminderNotifications,
      },
      () => {},
    );
  }

  editNotifications = async () => {
    const { navigation } = this.props;
    const validationResults = this.validator.validate(this.state);

    if (!validationResults.valid) {
      this.setState({
        processingRequest: { status: false, message: '' },
        errors: validationResults.errors,
      });

      return;
    }

    this.setState({
      processingRequest: { status: true, message: 'Updating...' },
    });

    const {
      announcementNotifications,
      eventNotifications,
      eventReminderNotifications,
    } = this.state;

    const { setUser, setMessage } = this.context;

    const updateUserResponse = await UserApi.updateSettings({
      settings: {
        announcementNotifications,
        eventNotifications,
        eventReminderNotifications,
      },
    });

    if (updateUserResponse.error) {
      setMessage('Unable to update notifications.');
      alert('Unable to update notifications.');
      console.log(updateUserResponse.error);
      return;
    }
    setMessage('Notifications updated.');

    this.setState({
      processingRequest: {
        status: true,
        message: 'Notification settings updated!',
      },
    });

    setUser(updateUserResponse.data);
    navigation.goBack();
  };

  render() {
    const {
      processingRequest,
      errors,
      announcementNotifications,
      eventNotifications,
      eventReminderNotifications,
    } = this.state;

    return (
      <Center>
        <Container style={{ width: '100%' }}>
          <Form style={{ width: '100%' }}>
            <Form.Select
              label="Announcements"
              options={announcementNotificationOptions}
              value={announcementNotifications}
              onChange={(value) => updateStateAndClearErrors(
                this,
                'announcementNotifications',
                value,
              )}
              error={errors.announcementNotifications}
            />
            <Form.Select
              label="Events"
              options={eventNotificationOptions}
              value={eventNotifications}
              onChange={(value) => updateStateAndClearErrors(this, 'eventNotifications', value)}
              error={errors.eventNotifications}
            />
            <Form.Select
              label="Event Reminders"
              options={eventReminderNotificationOptions}
              value={eventReminderNotifications}
              onChange={(value) => updateStateAndClearErrors(
                this,
                'eventReminderNotifications',
                value,
              )}
              error={errors.eventReminderNotifications}
            />
            <Form.Button
              text={
                processingRequest.status ? processingRequest.message : 'Save'
              }
              onPress={this.editNotifications}
            />
          </Form>
        </Container>
      </Center>
    );
  }
}
