import React from 'react';
import {
  FlatList,
  View,
} from 'react-native';
import {
  Text,
  Avatar,
  VStack,
  Select,
} from 'native-base';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import colors from '../util/colors';
import UserContext from '../util/UserContext';
import ClubsApi from '../api/ClubsApi';
import AddAdminButton from '../components/AddAdminButton';
import defaultProfileImage from '../assets/images/DefaultUserImage.png';

export default class AdminList extends React.Component {
    static contextType = UserContext;

    static yearToString(year) {
      switch (year) {
        case 2024:
          return '1st Year';
        case 2023:
          return '2nd Year';
        case 2022:
          return '3rd Year';
        case 2021:
          return '4th Year';
        case 2020:
          return '5th Year';
        default:
          return `${year}th Year`;
      }
    }

    constructor(props) {
      super(props);
      this.state = {
        isAdmin: false,
        club: null,
      };
    }

    async componentDidMount() {
      const { route } = this.props;
      const { club } = route.params;
      const { user } = this.context;
      const admins = await ClubsApi.getAdmins(club._id);
      this.setState({ club: { ...club, admins } });
      if ((admins.map((admin) => admin._id)).includes(user._id)) {
        this.setState({ isAdmin: true });
      }
    }

    setAdmins = (updatedAdmins) => {
      const { route } = this.props;
      const { updateClubState } = route.params;
      const { club } = this.state;

      const updatedClub = {
        ...club,
        admins: updatedAdmins,
      };

      this.setState({ club: updatedClub });
      updateClubState(updatedClub);
    }

    removeAdminHandler = (adminId) => {
      const { club } = this.state;

      const updatedAdmins = club?.admins?.filter((admin) => admin._id !== adminId);

      this.handleRemove(club, updatedAdmins);
      this.setAdmins(updatedAdmins);
    };

    handleRemove = async (club, updatedAdmins) => {
      const authResponse = await ClubsApi.updateClub(club._id, { ...club, admins: updatedAdmins });
      return authResponse;
    }

    render() {
      const {
        isAdmin, club,
      } = this.state;

      return (
        <VStack style={{ paddingTop: 20, width: '100%', height: '100%' }}>
          <FlatList
            data={club?.admins}
            style={{ width: '100%' }}
            renderItem={({ item }) => (
              <View
                style={{
                  width: '100%',
                  marginBottom: '3%',
                  paddingHorizontal: 20,
                }}
              >
                <View style={{
                  width: '100%',
                  display: 'flex',
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  borderBottomColor: colors.grayScale9,
                  borderBottomWidth: 0.3,
                  paddingBottom: '2%',
                }}
                >
                  <View
                    style={{
                      display: 'flex',
                      flexDirection: 'row',
                      alignItems: 'center',
                      backgroundColor: 'white',
                    }}
                  >
                    <Avatar
                      size="md"
                      style={{ margin: '2%' }}
                      source={item.profileImage ? { uri: item.profileImage } : defaultProfileImage}
                      backgroundColor="white"
                    />
                    <View
                      style={{
                        marginLeft: '3%',
                      }}
                    >
                      <Text>
                        {item.name.first}
                        {' '}
                        {item.name.last}
                      </Text>
                      <Text style={{ color: colors.grayScale9, fontSize: 14 }}>
                        {AdminList.yearToString(item.year) }
                    &nbsp;&#183;&nbsp;
                        { item.major }
                      </Text>
                    </View>
                  </View>
                  {isAdmin ? (
                    <View>
                      <Select
                        placeholder="Select an option"
                        width="100%"
                        borderWidth={0}
                        dropdownIcon={(
                          <MaterialCommunityIcons
                            name="account-minus-outline"
                            size={30}
                            color={colors.grayScale6}
                          />
                          )}
                      >
                        <Select.Item
                          label={`Remove ${item.name.first} ${item.name.last}`}
                          // Using onPressIn to add logic prior to onPress, which closes the dropdown menu by default
                          onPressIn={() => {
                            this.removeAdminHandler(item._id);
                          }}
                        />
                        <Select.Item
                          label="Cancel"
                        />
                      </Select>
                    </View>
                  ) : null}
                </View>
              </View>
            )}
          />
          {club && <AddAdminButton club={club} setAdmins={this.setAdmins} />}
        </VStack>
      );
    }
}
