import React from 'react';
import { NavigationContainer, DefaultTheme } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { Ionicons } from '@expo/vector-icons';

import SignupScr from '../screens/SignupScr';
import SigninScr from '../screens/SigninScr';
import ForgotPasswordScr from '../screens/ForgotPasswordScr';
import ForgotPasswordVerification from '../screens/ForgotPasswordVerification';
import EmailVerificationScr from '../screens/EmailVerificationScr';
import ClubfinityStack from './helpers/ClubfinityStack';
import { navigationRef } from './helpers/resetNavigationHelper';
import AuthScr from '../screens/AuthScr';

const Stack = createNativeStackNavigator();
const AuthNav = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

const getOptions = (barLabel, iconName) => ({
  tabBarIcon: ({ color, size }) => (
    <Ionicons name={iconName} size={size} color={color} />
  ),
  tabBarLabel: barLabel,
});

const getTabScreenForStack = (stackName, iconName) => (
  <Tab.Screen
    options={getOptions(stackName, iconName)}
    name={`${stackName}Stack`}
    children={() => <ClubfinityStack name={stackName} />}
  />
);

const AuthStack = () => (
  <AuthNav.Navigator
    initialRouteName="SignIn"
    screenOptions={{
      headerShown: false,
    }}
  >
    <AuthNav.Screen name="SignIn" component={SigninScr} />
    <AuthNav.Screen name="SignUp" component={SignupScr} />
    <AuthNav.Screen name="ForgotPassword" component={ForgotPasswordScr} />
    <AuthNav.Screen
      name="ForgotPasswordVerification"
      component={ForgotPasswordVerification}
    />
    <AuthNav.Screen name="EmailVerification" component={EmailVerificationScr} />
  </AuthNav.Navigator>
);

const AppStack = () => (
  <Tab.Navigator
    initialRouteName="HomeStack"
    screenOptions={{
      headerShown: false,
    }}
  >
    {getTabScreenForStack('Home', 'md-home')}
    {getTabScreenForStack('Discover', 'md-search')}
    {getTabScreenForStack('Calendar', 'md-calendar')}
    {getTabScreenForStack('Profile', 'md-happy')}
  </Tab.Navigator>
);

export default function AppNavigator() {
  const MyTheme = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      background: 'rgb(255, 255, 255)',
    },
  };

  return (
    <NavigationContainer theme={MyTheme} ref={navigationRef}>
      <Stack.Navigator
        initialRouteName="LoadState"
        screenOptions={{
          headerShown: false,
        }}
      >
        <Stack.Screen name="LoadState" component={AuthScr} />
        <Stack.Screen name="AppStack" component={AppStack} />
        <Stack.Screen name="AuthStack" component={AuthStack} options={{ animation: 'fade' }} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
