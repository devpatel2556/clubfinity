import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import CalendarScr from '../../screens/CalendarScr';
import HomeScr from '../../screens/HomeScr';
import DiscoverScr from '../../screens/DiscoverScr';
import ProfileScr from '../../screens/ProfileScr';
import ClubScr from '../../screens/ClubScr';
import EditProfile from '../../screens/EditProfile';
import EditNotificationsScr from '../../screens/EditNotificationsScr';
import SubmitClubScr from '../../screens/SubmitClubScr';
import SettingScr from '../../screens/SettingScr';
import EventList from '../../screens/EventList';
import AnnouncementList from '../../screens/AnnouncementList';
import EventCreation from '../../screens/EventCreation';
import CreateAnnouncementScr from '../../screens/CreateAnnouncementScr';
import AdminList from '../../screens/AdminList';
import EditClub from '../../screens/EditClub';
import EditEvent from '../../screens/EditEvent';
import EventScr from '../../screens/EventScr';
import AnnouncementScr from '../../screens/AnnouncementScr';
import EditAnnouncement from '../../screens/EditAnnouncements';
import ReportBugScr from '../../screens/ReportBugScr';
import DeleteUserAccountScr from '../../screens/DeleteUserAccountScr';
import SigninScr from '../../screens/SigninScr';
import googleCalScr from '../../screens/googleCalServiceScr';

const EventPages = {
  EventScr,
  EditEvent,
};

const ClubPages = {
  ClubScr,
  EditClub,
  CreateAnnouncementScr,
  EventCreation,
  EventList,
  AnnouncementList,
  AnnouncementScr,
  EditAnnouncement,
  AdminList,
  googleCalScr,
};

const ProfilePages = {
  EditProfile,
  EditNotificationsScr,
  SettingScr,
  SubmitClubScr,
  ReportBugScr,
  DeleteUserAccountScr,
  SigninScr,
};

const getStackPages = (pageName) => {
  if (pageName === 'Profile') {
    return {
      Profile: ProfileScr,
      ...Object.assign(ProfilePages),
      ...Object.assign(ClubPages),
      ...Object.assign(EventPages),
    };
  }
  if (pageName === 'Home') {
    return {
      Home: HomeScr,
      ...Object.assign(EventPages),
    };
  }
  if (pageName === 'Discover') {
    return {
      Discover: DiscoverScr,
      ...Object.assign(ClubPages),
      ...Object.assign(EventPages),
    };
  }
  if (pageName === 'Calendar') {
    return {
      Calendar: CalendarScr,
      ...Object.assign(EventPages),
    };
  }
  return Error('Error loading navigation frames.');
};

const ICON_SIZE = 30;
const ICON_COLOR = 'white';

function getHeaderRight(pageName, navigation) {
  return pageName === 'Profile'
    ? {
      headerRight: () => <HeaderIconComponent navigation={navigation} />,
    }
    : {};
}

const HeaderIconComponent = (props) => {
  const { navigation } = props;
  return (
    <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
      <TouchableOpacity
        onPress={() => navigation.navigate('Settings')}
        style={{
          paddingTop: '2%',
          paddingRight: '2%',
          alignSelf: 'flex-end',
        }}
      >
        <Ionicons name="md-settings" size={ICON_SIZE} color={ICON_COLOR} />
      </TouchableOpacity>
    </View>
  );
};

const screenName = {
  Profile: 'Profile',
  Home: 'Home',
  Discover: 'Discover',

  EventScr: 'Event',
  EditEvent: 'Edit Event',

  ClubScr: 'Club',
  Calendar: 'Calendar',
  EditClub: 'Edit Club',
  CreateAnnouncementScr: 'Create Announcement',
  EventCreation: 'Create Event',
  EventList: 'Events',
  AnnouncementList: 'Announcements',
  AnnouncementScr: 'Announcement',
  EditAnnouncement: 'Edit Announcement',
  AdminList: 'Admins',

  EditProfile: 'Edit Profile',
  EditNotificationsScr: 'Edit Notifications',
  SettingScr: 'Settings',
  SubmitClubScr: 'Submit Club',
  ReportBugScr: 'Report Bug',
  DeleteUserAccountScr: 'Delete',
  SigninScr: 'SignIn',
  googleCalScr: 'Google Calendar',
};

const generateStack = (Stack, pageName) => {
  const stackPages = getStackPages(pageName);
  // console.log(`${Stack} ${pageName}`);
  return Object.keys(stackPages).map(
    (name) => (
      <Stack.Screen
        key={name}
        name={screenName[name]}
        options={({ navigation }) => (getHeaderRight(name, navigation))}
        component={stackPages[name]}
      />
    ),
  );
};

export default generateStack;
