import Constants from 'expo-constants';

// eslint-disable-next-line no-undef
const env = __DEV__ ? 'development' : 'production';
const devUrl = Constants.expoConfig.extra.API_URL || 'https://clubfinity.staging.ufsec.org/';

const config = {
  development: {
    url: devUrl,
  },
  production: {
    url: 'http://clubfinity.prod.ufsec.org/',
  },
};

export default config[env];
