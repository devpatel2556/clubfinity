For a nicer looking guide with pictures, go to the following Google Doc.
https://docs.google.com/document/d/1EQsPDKnuqfFh9Tl8Npn8GW0r70GQrrifBtk-M0-9qws/edit?usp=sharing

Deploying Clubfinity is very easy with the use of GitLab CI/CD which automates almost all of the process. 
However, the first time Clubfinity is deployed (or when it needs to be redeployed manually for some reason) 
there are a few steps that need to be done. It is also good to go through it once to understand what is 
going on behind the scenes of the pipeline.

There are three sections to this guide:
Backend = deploying the backend server for the first time
Frontend = submitting new versions of the frontend to the app stores
Pipeline = understanding what the ‘.gitlab-ci.yml’ file is doing


Backend
    1. Create a google cloud account with a free trial (requires a credit card).
    
    2. Create a new project called Clubfinity.
    
    3. Enable the container registry API.
    
    4. Create a service account and generate a new json key. Downlaod the json key.
    
    5. Build a new backend image and push it to container registry
    (this step can be accomplished by simply running the "Build API" job on the GitLab pipeline)
        - In order for the “Build API” job to work, we need to give GitLab three variables so that
        our pipeline can authenticate itself to Google and connect to our newly created “Google Project”.
        - Gather the Project ID, Project Zone, and the GCLOUD_SERVICE_KEY from earlier.
        - Go to settings → CI/CD → Variables, and put these variables here. You can override the old ones.
        - Then, click run job.

        - If you look at the ‘.gitlab-ci.yml’ file, you’ll see what this job is doing. 
        - It is basically just doing <docker login>, <docker build>, and <docker push>.
        - You could manually run these commands if you prefer.
    
    6. Wait a few minutes for the image to show up on our google cloud console
    
    Cloud Run - Deploy Container
        - We are going to do steps (7 - 17) twice because we ultimately want two deployed containers.
        - One container for staging and one container for prod. 
        - When you get to step 8, make sure you use the environment variables for whichever container you are deploying. 
            - For example, when deploying the prod container, use the PROD_DATABASE_URL, not the STAGING_DATABASE_URL

    7. Link Cloud Run with our new image
        - Enable Cloud Run
        - Click Create Service
        - Select the Clubfinity Image you created in step 5
        - Make sure to change the container port to 8080 (because this is what we specified in our clubfinity/Backend/Dockerfile)

    8. Add the environment variables
        - Gather all remaining environment variables that were not used in step 5. 
        - We need to give them to our cloud run container. 
        - Some of the variables are:
            - DATABASE_URL (Staging and Prod)
            - JWT_SECRET
            - MAILGUN_API_KEY
            - MAILGUN_PUBLIC_KEY
            - NODE_ENV
            - GOOGLE_APPLICATION_CREDENTIALS
            - GOOGLE_CAL_API_KEY
            - Image hosting variables
            - Discord service variables

    9. Click allow unauthenticated invocations and click create.
        - After a few minutes, you will find the url which you can navigate to the newly deployed Clubfinity backend


    Cloud Run - DNS Configuration
    This cloud run generated url is ugly. We have a UFSEC domain, so we will want our API to use this domain.

    10. In the Google Cloud Run main dashboard (where it lists running containers), select “Manage Custom Domains“ at the top bar
    11. Click the “Add Mapping“ button 
    12. Select the appropriate running container (staging or prod)
    13. Select the verified domain (ufsec.org)
        - If not available, you will have to add a new verified domain (it shows ownership of the domain) 
    14. Type out the specific sub-domain wanted for that running container’s url
    15. Add new DNS record for subdomain
        - CNAME record w/ value as “ghs.googlehosted.com“
    FYI: Propagation took about 1 hour for the custom domains to be active

Frontend
    All you gotta know is the EXPO IS AWESOME
    
    Background:
        - Clubfinity is an “Expo Managed Workflow,” as opposed to a “Bare React App.” 
          What this means is we lean on Expo to do a lot of the heavy lifting for us,
          specifically when it comes to communicating with the phone’s OS such as 
          notifications and permissions.
        - Most importantly, we depend on expo to build our app for both iOS and android.
    
    2 magic commands (eas = expo application service)
        - eas build = Build the app for iOS and Android
            1. Sign in to the UF SEC Expo account
            2. Update app.config.js so the app version is incremented (upload will fail if the same version is used)
            3. Paste the google-services API key from the google play store account into google-services.json
            4. Run “eas build” → all → follow the on-screen steps
            5. Monitor / debug the build process on expo dashboard

        - eas submit = Submit the build to the app store
            - Google Play Store
                1. Sign in to UF SEC google play store dev account
                2. Download the android build from expo
                3. Go to the play store dashboard and click “create new release”
                4. When prompted for an app bundle, drag and drop the downloaded bundle there
                5. Publish to internal tests and eventually submit for review to the app store!
            
            - Apple app store
                1. Run “eas submit” → select iOS 
                2. Sign in to the UF SEC apple dev account
                3. Wait for the build to appear on the apple dev dashboard
                4. Launch the app for beta testing on “TestFlight”
                5. Submit the app for review!


Pipeline
    - Once you have deployed the frontend and backend once, the pipeline automates the process for future updates.
    - To better understand this process, it is helpful to understand the basics of docker.
        - Each job has a defined image. This is the name of the Docker image that GitLab uses to run the job.
    - Let's walk through the 7 jobs that our pipeline has:
        1. dependencies = Updates the node_modules folder for both the backend and frontend
            - We use GitLab's caching system so that we don't run <npm ci> unnecessarily
            - Notice we have a key for both the frontend and backend, because they both have a node_modules folder
            - Note this job only runs on master & merge_requests, so that development branches don't mess with the pipeline's node_modules

        2. test = This job simply runs the backend unit tests and the linter on both the frontend and backend
            - This job only runs on master & merge_requests, since we only care about code quality when it is time to merge.

        3. build = This job corresponds to step 5 of the backend deployment. It basically does three things.
            - It authenticates itself to the google cloud system.
            - It builds the image of our backend, using 'Backend/Dockerfile' as the blueprint.
            - It pushes this image to the google cloud container registry.

        4. deploy staging = This job tells cloud run to re-deploy the staging container, using the latest image.
        5. deploy prod = This job tells cloud run to re-deploy the prod container, using the latest image.

        6. build-frontend = This job runs the eas build command
            - The eas build command will continue running until the build is finished. However, this can take a long time.
                - So, we give it a timeout of 4 minutes and allow it fail (timing out a job is considered a failed job)
            - Once the command is run, you can moniter the progress on the UFSEC expo.dev account under builds

        7. deploy-ios = This job runs the eas submit command, specifically for iOS
            - The reason we don't have a deploy-androdi is because it is very simple to submit the android build. 
                - With android, you can easily download the .apk build and upload to the Google Play Store.
                - With iOS, you need to have XCode installed, or run the eas submit command.
