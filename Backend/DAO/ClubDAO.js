const Club = require('../Model/Club').Model;
const { NotFoundError } = require('../util/errors/validationError');
const { limitedUserModelFields } = require('../util/userUtil');
const userDAO = require('./UserDAO');

exports.getAll = async () => await Club.find({})
  .populate({
    path: 'admins',
    model: 'User',
    select: limitedUserModelFields,
  })
  .exec();

exports.get = async (id) => {
  const club = await Club.findById(id).populate({
    path: 'admins',
    model: 'User',
    select: limitedUserModelFields,
  }).exec();

  if (!club) throw new NotFoundError();

  return club;
};

exports.getByAdminId = async (userId) => Club.find({ admins: userId })
  .populate({
    path: 'admins',
    model: 'User',
    select: limitedUserModelFields,
  });

exports.isAdmin = async (userId, clubId) => {
  const clubs = await Club.find({ _id: clubId, admins: userId });
  return clubs.length === 1;
};

exports.exists = async (id) => {
  try {
    return await Club.exists({ _id: id });
  } catch (error) {
    return false;
  }
};

exports.nameExists = async (name) => {
  try {
    return await Club.exists({ name });
  } catch (error) {
    return false;
  }
};

exports.create = async (clubParams) => {
  if (await Club.exists({ name: clubParams.name })) {
    throw Error('Club name already exists');
  }

  const newClub = await new Club(clubParams).save();

  await Promise.all(clubParams.admins.map(async (admin) => {
    await userDAO.update(admin, { $addToSet: { clubs: newClub._id } });
  }));

  return newClub;
};

exports.update = async (id, updateData) => {
  await Club.findOneAndUpdate({ _id: id }, updateData, {
    upsert: true,
    useFindAndModify: false,
  });
  return exports.get(id);
};

exports.delete = async (id) => {
  const club = await Club.findByIdAndDelete(id);
  if (!club) throw new NotFoundError();

  return club;
};

exports.deleteAll = async () => {
  await Club.deleteMany();
};
