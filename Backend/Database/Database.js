const mongoose = require('mongoose');
const config = require('../Config/config');

const { sendServiceTicket } = require('../Services/SlackService');

const url = config.database;

mongoose.connection.on('connected', () => {
  console.log(`Database connection open to ${url}`);
  sendServiceTicket(true);
});

mongoose.connection.on('error', (error) => {
  console.log(`Mongoose connection error: ${error}`);
  sendServiceTicket(false);
});

mongoose.connection.on('disconnected', () => {
  console.log('Mongoose default connection disconnected');
  sendServiceTicket(false);
});

process.on('SIGINT', (error, data) => {
  console.log(`Disconnected by ${error} ${data}`);
  mongoose.connection.close(() => {
    console.log('Mongoose default connection disconnected through app termination');
    process.exit(0);
  });
});

exports.connect = () => {
  mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true });
};

exports.disconnect = () => {
  mongoose.disconnect();
};
