require('dotenv').config();
require('./Auth/passport');
const express = require('express');
const passport = require('passport');
const cron = require('node-cron');
const userRoutes = require('./Routes/UserRoutes');
const healthCheckRoute = require('./Routes/HealthCheckRoute');
const eventRoutes = require('./Routes/EventRoutes');
const clubRoutes = require('./Routes/ClubRoutes');
const announcementRoutes = require('./Routes/AnnouncementRoutes');
const ticketRoutes = require('./Routes/TicketRoutes');
const imageRoutes = require('./Routes/ImageRoutes');
const authRoute = require('./Routes/AuthRoutes');
const config = require('./Config/config');
const database = require('./Database/Database');
const { EmailService } = require('./Services/EmailService');
const { FakeEmailService } = require('./Services/FakeEmailService');
const { scheduledJobs } = require('./Services/scheduledJobs');
const { discordLogin } = require('./Services/Discord/BotLogin');

const app = express();

app.use(passport.initialize());
app.use(passport.session());

app.use(express.urlencoded({
  extended: true,
}));
app.use(express.json());

app.use('/api/users', userRoutes);
app.use('/api/events', eventRoutes);
app.use('/api/clubs', clubRoutes);
app.use('/api/announcements', announcementRoutes);
app.use('/api/tickets', ticketRoutes);
app.use('/api/images', imageRoutes);
app.use('/auth', authRoute);
app.use('/', healthCheckRoute);

database.connect();

global.emailService = config.email ? new EmailService() : new FakeEmailService();

if (config.discord) discordLogin();

const server = app.listen(config.port, () => {
  console.log(`Now listening on port ${config.port}`);
});

// Any jobs that need to be run daily (at midnight) go here
cron.schedule('0 0 0 * * *', () => {
  if (process.env.NODE_ENV === 'production') {
    scheduledJobs();
  }
});

function stop() {
  console.log('stopping');
  server.close();
  database.disconnect();
  process.exit(); // Make sure the app doesn't hang
}

module.exports = app;
module.exports.stop = stop;
