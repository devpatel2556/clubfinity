const passport = require('passport');
const express = require('express');
const clubController = require('../Controllers/ClubController');

const router = express.Router();

router.get('/', passport.authenticate('loggedIn', { session: false }),
  clubController.getMultiple);
router.get('/exists', passport.authenticate('loggedIn', { session: false }), clubController.checkClubExists);
router.get('/:id', clubController.get);
router.put('/:id', passport.authenticate('loggedIn', { session: false }),
  clubController.validate('validateBaseClubInfo'),
  clubController.update);
router.patch('/:id/google-cal-events', passport.authenticate('loggedIn', { session: false }), clubController.fillGoogleCalEvents);
router.get('/:calID/google-cal-check', passport.authenticate('loggedIn', { session: false }), clubController.checkGoogleCalID);
router.put('/:id/add-admin', passport.authenticate('loggedIn', { session: false }), clubController.addAdmin);

module.exports = router;
