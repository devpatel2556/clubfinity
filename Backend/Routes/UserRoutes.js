const passport = require('passport');
const express = require('express');
const userController = require('../Controllers/UserController');

const router = express.Router();

router.post(
  '/register',
  userController.validate('validateFullUserInfo'),
  userController.register,
);
router.post(
  '/resend',
  userController.resendEmailVerificationCode,
);
router.post(
  '/verify',
  userController.verifyEmailCode,
);

router.get(
  '/',
  passport.authenticate('loggedIn', { session: false }),
  userController.get,
);
router.put(
  '/',
  passport.authenticate('loggedIn', { session: false }),
  userController.validate('validateBaseUserInfo'),
  userController.update,
);
router.patch(
  '/',
  passport.authenticate('loggedIn', { session: false }),
  userController.validate('validatePushToken'),
  userController.updatePushToken,
);
router.patch(
  '/push-token',
  passport.authenticate('loggedIn', { session: false }),
  userController.validate('validatePushToken'),
  userController.updatePushToken,
);
router.patch(
  '/user-settings',
  passport.authenticate('loggedIn', { session: false }),
  userController.validate('validateUserSettings'),
  userController.updateUserSettings,
);
router.patch(
  '/clubs/:id',
  passport.authenticate('loggedIn', { session: false }),
  userController.validate('validateClubId'),
  userController.updateClubFollowingState,
);
router.post(
  '/forgot-password',
  userController.forgotPasswordEmail,
);
router.post(
  '/reset-password',
  userController.resetPassword,
);
router.delete(
  '/:userId',
  passport.authenticate('loggedIn', { session: false }),
  userController.delete,
);

module.exports = router;
