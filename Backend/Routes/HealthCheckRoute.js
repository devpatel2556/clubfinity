const express = require('express');
const { catchErrors } = require('../util/httpUtil');

const router = express.Router();

router.get(
  '/',
  async (req, res) => catchErrors(res, async () => 'Clubfinity backend is running!'),
);

module.exports = router;
