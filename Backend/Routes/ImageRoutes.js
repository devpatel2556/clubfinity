const passport = require('passport');
const express = require('express');
const multer = require('multer');
const ImageUploadController = require('../Controllers/ImageUploadController');

const router = express.Router();

// memoryStorage() creates a memory storage engine that stores file in memory as Buffer object
const storage = multer.memoryStorage();
// multer wrapper creates middleware that will handle file uploads based on configuration provided
// 'storage' is passed in, which tells multer to use in-memory storage engine to handle file
const upload = multer({ storage });

// When 'upload.single('image')' is passed in, multer will store the uploaded file
// in the req.file object, which is accessed in the controller
router.post('/:type', passport.authenticate('loggedIn', { session: false }), upload.single('image'), ImageUploadController.upload);

module.exports = router;
