const config = require('../../Config/config');
const { discordTicketClient } = require('./Tickets/TicketManager');

const discordLogin = () => {
  discordTicketClient.login(config.discord.tickets.botToken);
};

module.exports = {
  discordLogin,
};
