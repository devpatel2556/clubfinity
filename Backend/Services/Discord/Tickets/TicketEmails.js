const discordEmailService = async (type, data) => {
  const emailData = data;

  switch (type) {
    case 'ticketResolved':
      emailData.subject = 'Clubfinity Bug Report';
      emailData.body = `
Hello ${emailData.name},
        
Thank you for the bug report! Your issue will be resolved in the next Clubfinity update.
        
Bug Description:
${emailData.bugDescription}
        
- The Clubfinity Team
        `;
      break;
    case 'clubCreated':
      emailData.subject = 'Clubfinity Submit Club Request';
      emailData.body = `
Hello ${emailData.name},
        
Congratulations! Your submission request for the club: "${emailData.clubName}" has been accepted!
        
You can now view, and manage, your new club within the Clubfinity app.
        
- The Clubfinity Team
        `;
      break;
    case 'clubRejected':
      emailData.subject = 'Clubfinity Submit Club Request';
      emailData.body = `
Hello ${emailData.name},
        
Unfortunately, your submission request for the club: "${emailData.clubName}" has been denied.
        
Here is the feedback from our team:
${emailData.feedback}
        
- The Clubfinity Team
        `;
      break;
    case 'clubCreationError':
      emailData.subject = 'Clubfinity Submit Club Request';
      emailData.body = `
Hello ${emailData.name},
        
Unfortunately, there has been an error with the approval of your recent club request for: "${emailData.clubName}".
        
While the Clubfinity team had accepted your submission, it seems that an error has occured within our system. We are sorry for this disturbance, and suggest that you re-submit a club creation request based on the feedback below.

Error Feedback:
${emailData.error}
        
- The Clubfinity Team
        `;
      break;
    default:
      break;
  }

  await global.emailService.send(
    emailData.email,
    emailData.subject,
    emailData.body,
  );
};

module.exports = {
  discordEmailService,
};
