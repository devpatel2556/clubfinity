const {
  EmbedBuilder,
  ActionRowBuilder,
  ButtonBuilder,
  ButtonStyle,
  ModalBuilder,
  TextInputBuilder,
  TextInputStyle,
} = require('discord.js');
const { htmlToText } = require('html-to-text');

const botAvatar = 'https://cdn.discordapp.com/avatars/1075609239428538440/6e59002204edc78140e9fd23f18cfaa7.png?size=256';
const clubfinityColor = 0x0099FF;

// Discord embed modifications for tickets in "loading" state
const setEmbedToLoading = async (interaction) => {
  const buttons = new ActionRowBuilder()
    .addComponents(
      new ButtonBuilder()
        .setCustomId('fixed')
        .setLabel('Updating ticket..')
        .setStyle(ButtonStyle.Secondary)
        .setDisabled(true),
    );

  await interaction.message.edit({ components: [buttons] });
  await interaction.deferUpdate();
};

// Discord embed template for bug tickets
const bugEmbed = (bug) => {
  const discordTimestamp = Math.floor(bug.timestamp.valueOf() / 1000);

  const embed = new EmbedBuilder()
    .setColor(clubfinityColor)
    .setTitle('New Bug Report!  🚨')
    .setFooter({ text: 'Powered by Clubfinity', iconURL: botAvatar })
    .setDescription(`Submitted at <t:${discordTimestamp}:f>`)
    .addFields(
      { name: 'Description', value: `\`\`\`${htmlToText(bug.bugData)}\`\`\`` },
      { name: 'Ticket #', value: `${bug._id}` },
    );

  const buttons = new ActionRowBuilder()
    .addComponents(
      new ButtonBuilder()
        .setCustomId('resolve')
        .setLabel('Resolved')
        .setStyle(ButtonStyle.Primary),
      new ButtonBuilder()
        .setCustomId('discard')
        .setLabel('Discard')
        .setStyle(ButtonStyle.Secondary),
    );

  return { embeds: [embed], components: [buttons] };
};

// Discord embed template for bug tickets after being resolved
const resolvedBugEmbed = (interaction) => {
  const embed = {
    ...interaction.message.embeds[0].data,
    title: 'Bug Resolved! 🛠️',
    description: `Ticket was closed by <@${interaction.user.id}>`,
    fields: [interaction.message.embeds[0].data.fields[0]],
  };

  const buttons = new ActionRowBuilder()
    .addComponents(
      new ButtonBuilder()
        .setCustomId('fixed')
        .setLabel('Resolved')
        .setStyle(ButtonStyle.Success)
        .setDisabled(true)
        .setEmoji('✅'),
    );

  return { embeds: [embed], components: [buttons] };
};

// Discord embed template for club creation tickets
const clubEmbed = (clubTicket) => {
  const discordTimestamp = Math.floor(clubTicket.timestamp.valueOf() / 1000);

  const embed = new EmbedBuilder()
    .setColor(clubfinityColor)
    .setTitle('New Club Submission!  📬')
    .setFooter({ text: 'Powered by Clubfinity', iconURL: botAvatar })
    .setDescription(`Submitted at <t:${discordTimestamp}:f>`)
    .setThumbnail(clubTicket.clubData.thumbnailUrl)
    .addFields(
      { name: 'Club Name', value: `${clubTicket.clubData.name}`, inline: true },
      { name: 'Club Category', value: `${clubTicket.clubData.category}`, inline: true },
      { name: 'Description', value: `\`\`\`${htmlToText(clubTicket.clubData.description)}\`\`\`` },
      { name: 'Ticket#', value: `${clubTicket._id}` },
    );

  if (clubTicket.clubData.slackLink) {
    embed.spliceFields(3, 0, { name: 'Slack', value: `[Link](${clubTicket.clubData.slackLink})`, inline: true });
  }
  if (clubTicket.clubData.instagramLink) {
    embed.spliceFields(3, 0, { name: 'Instagram', value: `[Link](${clubTicket.clubData.instagramLink})`, inline: true });
  }
  if (clubTicket.clubData.facebookLink) {
    embed.spliceFields(3, 0, { name: 'Facebook', value: `[Link](${clubTicket.clubData.facebookLink})`, inline: true });
  }
  if (clubTicket.clubData.googleCalLink) {
    embed.spliceFields(3, 0, { name: 'Google Calendar', value: `${clubTicket.clubData.googleCalLink}` });
  }

  const buttons = new ActionRowBuilder()
    .addComponents(
      new ButtonBuilder()
        .setCustomId('accept')
        .setLabel('Accept')
        .setStyle(ButtonStyle.Success),
      new ButtonBuilder()
        .setCustomId('deny')
        .setLabel('Deny')
        .setStyle(ButtonStyle.Danger),
    );

  return { embeds: [embed], components: [buttons] };
};

// Discord embed template for club creation tickets after being accepted
const acceptedClubEmbed = (interaction, err) => {
  let embed = null;
  let buttons = null;

  if (err) {
    embed = {
      ...interaction.message.embeds[0].data,
      title: `Error: ${err.message}  ⚠️`,
      description: `Ticket was closed by <@${interaction.user.id}>`,
      fields: [
        interaction.message.embeds[0].data.fields[0],
        interaction.message.embeds[0].data.fields[2],
      ],
      color: 0xFFD000,
    };

    buttons = new ActionRowBuilder()
      .addComponents(
        new ButtonBuilder()
          .setCustomId('error')
          .setLabel('User Alerted')
          .setStyle(ButtonStyle.Secondary)
          .setDisabled(true)
          .setEmoji('❕'),
      );
  } else {
    embed = {
      ...interaction.message.embeds[0].data,
      title: 'Submission Accepted!  👍',
      description: `Ticket was closed by <@${interaction.user.id}>`,
      fields: [
        interaction.message.embeds[0].data.fields[0],
        interaction.message.embeds[0].data.fields[2],
      ],
      color: 0x096D0B,
    };

    buttons = new ActionRowBuilder()
      .addComponents(
        new ButtonBuilder()
          .setCustomId('created')
          .setLabel('Created')
          .setStyle(ButtonStyle.Success)
          .setDisabled(true)
          .setEmoji('✅'),
      );
  }
  return { embeds: [embed], components: [buttons] };
};

// Discord embed template for club creation tickets after being rejected
const rejectedClubEmbed = (interaction, feedback) => {
  const embed = {
    ...interaction.message.embeds[0].data,
    title: 'Submission Denied!  👎',
    description: `Ticket was closed by <@${interaction.user.id}>`,
    fields: [
      interaction.message.embeds[0].data.fields[0],
      interaction.message.embeds[0].data.fields[2],
      feedback.length > 0 ? { name: 'Feedback', value: feedback } : null,
    ].filter(Boolean),
    color: 0xD32A2A,
  };

  const buttons = new ActionRowBuilder()
    .addComponents(
      new ButtonBuilder()
        .setCustomId('denied')
        .setLabel('Denied')
        .setStyle(ButtonStyle.Danger)
        .setDisabled(true)
        .setEmoji('⛔'),
    );

  return { embeds: [embed], components: [buttons] };
};

// Discord modal template for admin feedback after club creation refusal
const feedbackModal = () => {
  const modal = new ModalBuilder()
    .setCustomId('feedback')
    .setTitle('Ticket Feedback');

  const feedbackInput = new TextInputBuilder()
    .setCustomId('feedbackInput')
    .setLabel('Do you wish to provide any feedback?')
    .setStyle(TextInputStyle.Paragraph)
    .setMaxLength(500)
    .setPlaceholder('Leave empty for no feedback')
    .setRequired(false);

  const firstActionRow = new ActionRowBuilder().addComponents(feedbackInput);

  modal.addComponents(firstActionRow);
  return modal;
};

// Discord embed template for invalid/expired tickets
const invalidTicket = () => ({
  content: 'This ticket has been removed as it is no longer valid. You may delete this message.',
  embeds: [],
  components: [],
});

module.exports = {
  setEmbedToLoading,
  bugEmbed,
  resolvedBugEmbed,
  clubEmbed,
  acceptedClubEmbed,
  rejectedClubEmbed,
  feedbackModal,
  invalidTicket,
};
