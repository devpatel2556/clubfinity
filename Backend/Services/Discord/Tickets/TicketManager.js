const {
  Client, IntentsBitField,
} = require('discord.js');
const { htmlToText } = require('html-to-text');
const { NotFoundError } = require('../../../util/errors/notFoundError');
const config = require('../../../Config/config');
const userDAO = require('../../../DAO/UserDAO');
const clubDAO = require('../../../DAO/ClubDAO');
const ticketDAO = require('../../../DAO/TicketDAO');
const { discordEmailService } = require('./TicketEmails');
const {
  setEmbedToLoading,
  bugEmbed,
  resolvedBugEmbed,
  clubEmbed,
  acceptedClubEmbed,
  rejectedClubEmbed,
  feedbackModal,
  invalidTicket,
} = require('./TicketEmbeds');

// Build discord client to interact with the Clubfinity tickets bot
const discordTicketClient = config.discord ? new Client({
  intents: [
    IntentsBitField.Flags.Guilds,
    IntentsBitField.Flags.GuildMembers,
    IntentsBitField.Flags.GuildMessages,
    IntentsBitField.Flags.MessageContent,
  ],
}) : undefined;

// Sending initial bug ticket via Clubfinity bot
const sendBugTicket = async (bugTicket) => {
  if (!discordTicketClient) {
    console.log('Unable to process discord ticket request in staging envirionments');
    return;
  }

  const channel = await discordTicketClient.channels.fetch(config.discord.tickets.bugChannelId);
  channel.send(bugEmbed(bugTicket));
};

// Method to control admin feedback on bug ticket
const resolveBugTicket = async (interaction) => {
  try {
    // Defer update (give ACK to discord) + set interaction button as "loading"
    await setEmbedToLoading(interaction);

    const ticketId = interaction.message.embeds[0].data.fields[
      interaction.message.embeds[0].data.fields.length - 1
    ].value;
    const { bugData, user: userId } = await ticketDAO.get(ticketId);
    const user = await userDAO.get(userId);
    await ticketDAO.delete(ticketId);

    // Admin has discarded the bug ticket -> remove ticket all together
    if (interaction.customId === 'discard') {
      interaction.message.delete();
      return;
    }

    // Send bug resolution email to user who raised the concern
    discordEmailService('ticketResolved', {
      name: `${user.name.first} ${user.name.last}`,
      email: user.email,
      bugDescription: htmlToText(bugData),
    });

    interaction.editReply(resolvedBugEmbed(interaction));
  } catch (error) {
    if (error instanceof NotFoundError) {
      // Todo: handle scenario where a user has deleted their account. Should ticket still be valid?
      // https://www.notion.so/ufsec/Fix-discord-tickets-regarding-deleted-users-a178be6842544060892dc4528f7c335d?pvs=4
      interaction.editReply(invalidTicket());
    }
  }
};

// Sending initial club submission ticket via Clubfinity bot
const sendClubTicket = async (clubTicket) => {
  if (!discordTicketClient) {
    console.log('Unable to process discord ticket request in staging envirionments');
    return;
  }

  const channel = await discordTicketClient.channels.fetch(config.discord.tickets.clubChannelId);
  channel.send(clubEmbed(clubTicket));
};

// Method to control admin feedback on club submission ticket (accept, deny, feedback)
const resolveClubTicket = async (interaction) => {
  try {
    // Admin has accepted club submission
    if (interaction.customId === 'accept') {
      // Defer update (give ACK to discord) + set interaction button as "loading"
      await setEmbedToLoading(interaction);

      let newMessage = null;
      const ticketId = interaction.message.embeds[0].data.fields[
        interaction.message.embeds[0].data.fields.length - 1
      ].value;
      const { clubData, user: userId } = await ticketDAO.get(ticketId);
      const user = await userDAO.get(userId);
      await ticketDAO.delete(ticketId);

      const clubInfo = {
        ...clubData.toObject(),
        admins: [userId],
        description: htmlToText(clubData.description),
      };

      // Create new club
      await clubDAO.create(clubInfo)
        .then(() => {
          newMessage = acceptedClubEmbed(interaction);

          // Send club creation notice to user
          discordEmailService('clubCreated', {
            name: `${user.name.first} ${user.name.last}`,
            email: user.email,
            clubName: clubInfo.name,
          });
        })
        .catch((err) => {
          newMessage = acceptedClubEmbed(interaction, err);

          // Send club creation error notice to user (most likely due to club name already existing)
          discordEmailService('clubCreationError', {
            name: `${user.name.first} ${user.name.last}`,
            email: user.email,
            clubName: clubInfo.name,
            error: err.message,
          });
        });

      interaction.editReply(newMessage);
    // Admin has refused submission -> activate modal for optional feedback
    } else if (interaction.customId === 'deny') {
      interaction.showModal(feedbackModal());
    // Admin has finished giving feedback for club submission refusal
    } else if (interaction.customId === 'feedback') {
      // Defer update (give ACK to discord) + set interaction button as "loading"
      await setEmbedToLoading(interaction);

      const ticketId = interaction.message.embeds[0].data.fields[
        interaction.message.embeds[0].data.fields.length - 1
      ].value;
      const { clubData, user: userId } = await ticketDAO.get(ticketId);
      const user = await userDAO.get(userId);
      await ticketDAO.delete(ticketId);

      const feedback = interaction.fields.fields.get('feedbackInput').value;

      // Send feedback reasoning to user if given by Admin
      if (feedback.length > 0) {
        discordEmailService('clubRejected', {
          name: `${user.name.first} ${user.name.last}`,
          email: user.email,
          clubName: clubData.name,
          feedback,
        });
      }

      interaction.editReply(rejectedClubEmbed(interaction, feedback));
    }
  } catch (error) {
    if (error instanceof NotFoundError) {
      // Todo: handle scenario where a user has deleted their account. Should ticket still be valid?
      // https://www.notion.so/ufsec/Fix-discord-tickets-regarding-deleted-users-a178be6842544060892dc4528f7c335d?pvs=4
      interaction.editReply(invalidTicket());
    }
  }
};

// Listener for any interactions with the Clubfinity tickets bot
if (discordTicketClient) {
  discordTicketClient.on('interactionCreate', async (interaction) => {
    if (
      interaction.message.author.id === discordTicketClient.user.id
      && (interaction.isButton() || interaction.isModalSubmit())
    ) {
      if (interaction.channelId === config.discord.tickets.bugChannelId) {
        resolveBugTicket(interaction);
      } else if (interaction.channelId === config.discord.tickets.clubChannelId) {
        resolveClubTicket(interaction);
      }
    }
  });
}

module.exports = {
  discordTicketClient,
  sendBugTicket,
  sendClubTicket,
};
