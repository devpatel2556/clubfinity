const axios = require('axios');
const { Types } = require('mongoose');
const { DateTime } = require('luxon');
const { create, getByName } = require('../DAO/EventDAO');
const config = require('../Config/config');
const { ENV } = require('../Config/config');

const API_KEY = config.googleCalAPIKEY;
console.log('testing deployment of google calendar script');
console.log('google calendar api');
console.log(API_KEY);
const db = process.env.DATABASE_URL;
console.log('db url');
console.log(db);

// =======================================================
// fetches paginated event data from Google Calendar
// =======================================================
exports.getEventsPaginated = async (calendarId, nextPageToken = '') => {
  if (!API_KEY && ENV === 'development') {
    console.log('No api key provided for google calendar script');
    return true;
  }
  try {
    const res = await axios.get(
      `https://www.googleapis.com/calendar/v3/calendars/${calendarId}/events`,
      {
        params: {
          key: API_KEY,
          pageToken: nextPageToken,
        },
      },
    );
    return res.data;
  } catch (err) {
    console.error('Error from getEventsPaginated', err.message);
    return err;
  }
};

// =======================================================
// returns the events on the most recent page of calendar
// =======================================================
const getLatestEvents = async (calendarId) => {
  try {
    // eslint-disable-next-line
    let res = await this.getEventsPaginated(calendarId);
    // keep requesting events until on last page
    /* eslint-disable no-await-in-loop */
    while (res.nextPageToken) {
      // eslint-disable-next-line
      res = await this.getEventsPaginated(calendarId, res.nextPageToken);
    }
    return res.items;
  } catch (err) {
    console.error('Error from CalendarScrapper getLatestEventsPage:', err.message);
    return err;
  }
};

// =======================================================
// filters out past events and posts remaining events to DB
// =======================================================
const createEventsFromGCal = async (clubId, calendarId) => {
  try {
    let events = await getLatestEvents(calendarId);
    // filtering out past events
    events = events.filter((event) => {
      if (!event.start || !event.start.dateTime) {
        return false;
      }
      return (
        // todo fix issue with timezone. For now, harcode it
        DateTime.local().setZone('America/New_York')
        < DateTime.fromISO(event.start.dateTime)
      );
    });

    // posting new events
    events.forEach(async (event) => {
      // filtering out Google specific formatting for descriptions
      let eventDescription = '';
      if (event.description) {
        eventDescription = event.description.replace(/(<br>|<b>|<\/b>|<i>|<\/i>)/g, '');
        eventDescription = eventDescription.replace(/&nbsp;/g, ' ');
      }

      // creating new event and posting
      const newEvent = {
        name: event.summary || '',
        location: event.location || '',
        facebookLink: '',
        description: eventDescription,
        date: DateTime.fromISO(event.start.dateTime).toLocal(),
        goingUsers: [],
        uninterestedUsers: [],
        interestedUsers: [],
        club: new Types.ObjectId(clubId),
      };
      try {
        await getByName(newEvent.name);
      } catch (Error) {
        if (Error.httpErrorCode === 404) {
          // No event with this name was found for this club
          create(newEvent);
        }
      }
    });
  } catch (err) {
    console.error('Error from CalendarScrapper createEventsFromGCal:');
    console.log(err);
    return false;
  }
  return true;
};

// =======================================================
// posting events to the database based on the specified
// clubs' Google Calendars and club ids
// =======================================================
exports.CalendarScrapperService = async (clubID, calendarToCopyFrom) => {
  if (!API_KEY && ENV === 'development') {
    console.log('No api key provided for google calendar script');
    return true;
  }
  const resp = await createEventsFromGCal(clubID, calendarToCopyFrom);
  return resp;
  //   // WiSCE
  //   createEventsFromGCal(
  //     '99cb91bdc3464f14678934ca', // NOT CORRECT CLUB ID
  //     '291pbpr99guo0kl6md2g0nghk8@group.calendar.google.com'
  //   );

  //   // SHPE
  //   createEventsFromGCal('99ce7614c3464f14678934ca', 'calendar.shpeuf@gmail.com');
};
