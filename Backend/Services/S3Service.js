const { S3Client, PutObjectCommand } = require('@aws-sdk/client-s3');
const crypto = require('crypto');
const config = require('../Config/config');
const { ENV } = require('../Config/config');

const randomImageName = (bytes = 32) => crypto.randomBytes(bytes).toString('hex');

// handle image upload route
exports.uploadImage = async (type, buffer, mimetype) => {
  if (!config.s3 && ENV === 'development') {
    console.log('No api keys provided for s3 image hosting service. Default images will be used if needed');
    return null;
  }

  const {
    bucketName, bucketRegion, accessKey, secretKey,
  } = config.s3;

  const s3 = new S3Client({
    region: bucketRegion,
    credentials: {
      accessKeyId: accessKey,
      secretAccessKey: secretKey,
    },
  });

  const imageName = randomImageName();

  const params = {
    Bucket: bucketName,
    Key: `${type}/${imageName}`,
    Body: buffer,
    ContentType: mimetype,
  };
  await s3.send(new PutObjectCommand(params));
  const imagePath = `https://${bucketName}.s3.${bucketRegion}.amazonaws.com/${type}/${imageName}`;
  return imagePath;
};
