const { validationResult, body } = require('express-validator');
const clubDAO = require('../DAO/ClubDAO');
const userDAO = require('../DAO/UserDAO');
const eventDAO = require('../DAO/EventDAO');
const announcementDAO = require('../DAO/AnnouncementDAO');
const { ValidationError } = require('../util/errors/validationError');
const { catchErrors } = require('../util/httpUtil');
const { CalendarScrapperService, getEventsPaginated } = require('../Services/googleCalendarService');

const validateClubData = (req) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) throw new ValidationError(errors.array());
};

exports.getMultiple = async (req, res) => catchErrors(res, async () => {
  const { type } = req.query;
  switch (type) {
    case 'all':
      return clubDAO.getAll();
    case 'fromAdminId':
      return clubDAO.getByAdminId(req.userId);
    default:
      throw new Error(`Invalid type ${type}`);
  }
});

exports.get = async (req, res) => catchErrors(res, async () => {
  const { select } = req.query;
  const { id: clubId } = req.params;
  switch (select) {
    case 'all':
      return clubDAO.get(clubId);
    case 'posts':
      return {
        events: await eventDAO.getByClubs([clubId]),
        announcements: await announcementDAO.getByClubs([clubId]),
      };
    default:
      throw new Error(`Invalid select ${select}`);
  }
});

exports.update = async (req, res) => catchErrors(res, async () => {
  validateClubData(req);

  return clubDAO.update(req.params.id, req.body);
});

exports.addAdmin = async (req, res) => catchErrors(res, async () => {
  // User to add
  const user = await userDAO.getByEmail(req.body.email);

  // Club to add to
  const club = await clubDAO.get(req.params.id);

  if (user.clubs.filter((clubObj) => clubObj._id.toString() === req.params.id).length === 0) {
    throw new Error('User is not a member of the club');
  }

  if (!(await clubDAO.isAdmin(req.userId, req.params.id))) {
    throw new Error('User trying to add is not an admin');
  }

  if (await clubDAO.isAdmin(user._id, req.params.id)) {
    throw new Error('User is already an admin');
  }

  const newAdmin = {
    name: user.name,
    _id: user._id,
    major: user.major,
    year: user.year,
  };

  return await clubDAO.update(req.params.id, {
    admins: [...club.admins, newAdmin],
  });
});

exports.fillGoogleCalEvents = async (req, res) => catchErrors(res,
  async () => CalendarScrapperService(req.params.id, req.body.googleCal));

exports.checkGoogleCalID = async (req, res) => catchErrors(res,
  async () => {
    const googleCalRes = await getEventsPaginated(req.params.calID);
    if (googleCalRes.response && googleCalRes.response.status === 404) {
      return false;
    }
    return true;
  });

exports.checkClubExists = async (req, res) => catchErrors(res, async () => {
  const { name } = req.query;
  if (await clubDAO.nameExists(name)) {
    return false;
  }
  return true;
});

exports.delete = async (req, res) => catchErrors(res, async () => clubDAO.delete(req.params.id));

exports.validate = (type) => {
  switch (type) {
    case 'validateBaseClubInfo': {
      return [
        body('name', 'Club name does not exist').exists(),
        body('category', 'Club category does not exist').exists(),
        body(
          'description',
          'Description does not exist or is invalid',
        ).exists(),
      ];
    }
    case 'validateCreateClubInfo': {
      return [body('tags', 'Tags does not exist').exists()];
    }
    case 'validateUpdateClubInfo': {
      return [body('admins', 'Club admins does not exist').exists()];
    }
    default: {
      throw new Error('Invalid validator');
    }
  }
};
