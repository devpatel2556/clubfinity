const { catchErrors } = require('../util/httpUtil');
const ImageService = require('../Services/S3Service');

exports.upload = async (req, res) => catchErrors(res, async () => {
  const { type: useType } = req.params;
  const imagePath = await ImageService.uploadImage(useType, req.file.buffer, req.file.mimetype);
  return imagePath;
});
